A collection of tools to expedite creation of custom rigs in Blender.  

QuickRig Bone Panel:  
![quickrig_panel.png](https://bitbucket.org/repo/R8jeyK/images/996033723-quickrig_panel.png)  

  
QuickRig Edit Panel:  
![quickrig_edit_panel.png](https://bitbucket.org/repo/R8jeyK/images/35199121-quickrig_edit_panel.png)  

  
QuickRig Constraints Panel:  
![quickrig_constraints_panel.png](https://bitbucket.org/repo/R8jeyK/images/3338099300-quickrig_constraints_panel.png)