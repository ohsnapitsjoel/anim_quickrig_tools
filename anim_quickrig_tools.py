bl_info = { "version" : (0,1),
            "author" : "Joel Daniels",
            "category" : "Animation",
            "name" : "Quick Rigging Tools"}

import bpy
from mathutils import Vector, Euler, Quaternion

class QuickRigBoneLock( bpy.types.Operator):
    bl_label = "Transform Lock"
    bl_description = ""
    bl_idname = "quickrig.transform_lock"
    
    def execute( self, context):
        pbones = context.selected_pose_bones
        scene = context.scene
        quickrig = scene.quickrig
        
        for pb in pbones:
            if not quickrig.unlock:
                if quickrig.lock_loc:
                    if quickrig.lock_x:
                        pb.lock_location[0] = True
                    if quickrig.lock_y:
                        pb.lock_location[1] = True
                    if quickrig.lock_z:
                        pb.lock_location[2] = True
                if quickrig.lock_rot:
                    if quickrig.lock_x:
                        pb.lock_rotation[0] = True
                    if quickrig.lock_y:
                        pb.lock_rotation[1] = True
                    if quickrig.lock_z:
                        pb.lock_rotation[2] = True
                    if quickrig.lock_w and pb.rotation_mode == 'QUATERNION':
                        pb.lock_rotation_w = True
                if quickrig.lock_scale:
                    if quickrig.lock_x:
                        pb.lock_scale[0] = True
                    if quickrig.lock_y:
                        pb.lock_scale[1] = True
                    if quickrig.lock_z:
                        pb.lock_scale[2] = True
            else:
                if quickrig.lock_loc:
                    if quickrig.lock_x:
                        pb.lock_location[0] = False
                    if quickrig.lock_y:
                        pb.lock_location[1] = False
                    if quickrig.lock_y:
                        pb.lock_location[2] = False
                if quickrig.lock_rot:
                    if quickrig.lock_x:
                        pb.lock_rotation[0] = False
                    if quickrig.lock_y:
                        pb.lock_rotation[1] = False
                    if quickrig.lock_y:
                        pb.lock_rotation[2] = False
                    if quickrig.lock_w and pb.rotation_mode == 'QUATERNION':
                        pb.lock_rotation_w = False
                if quickrig.lock_scale:
                    if quickrig.lock_x:
                        pb.lock_scale[0] = False
                    if quickrig.lock_y:
                        pb.lock_scale[1] = False
                    if quickrig.lock_y:
                        pb.lock_scale[2] = False
        return {'FINISHED'}


class QuickRigBoneDeform( bpy.types.Operator):
    bl_label = "Deform Toggle"
    bl_description = ""
    bl_idname = "quickrig.deform_toggle"
    
    def execute( self, context):
        pbones = context.selected_pose_bones
        scene = context.scene
        quickrig = scene.quickrig
        
        for pb in pbones: 
            pb.bone.use_deform = quickrig.deform
        return {'FINISHED'}
    
    
class QuickRigSetShape( bpy.types.Operator):
    bl_label = "Set Shape"
    bl_description = ""
    bl_idname = "quickrig.set_shape"
    
    def execute( self, context):
        pbones = context.selected_pose_bones
        scene = context.scene
        quickrig = scene.quickrig
        
        for pb in pbones: 
            if quickrig.shape == "":
                pb.bone.show_wire = False
                pb.custom_shape = None
            else:
                pb.bone.show_wire = True
                pb.custom_shape = bpy.data.objects[ quickrig.shape]
        return {'FINISHED'}


class QuickRigRenameBones( bpy.types.Operator):
    bl_label = "Rename Bones"
    bl_description = ""
    bl_idname = "quickrig.rename_bones"
    
    def _filter_int( self, i):
        try: 
            int( i)
            return False
        except:
            return True
            
    def execute( self, context):
        pbones = context.selected_pose_bones
        scene = context.scene
        quickrig = scene.quickrig
        index = quickrig.name_index
        for pb in pbones: 
            name = pb.name
            if quickrig.remove_numbering:
                char_list = [char for char in pb.name]
                char_list = list( filter( self._filter_int, char_list))
                name = "".join( char_list)
                print( name)
                if name.endswith("."):
                    name = name[:-1]
            name = name.replace( quickrig.replace_text, quickrig.with_text)
            print( name)
            if quickrig.prepend != "":
                name = quickrig.prepend + name
                print( name)
            if quickrig.append != "":
                name += quickrig.append
                print( name)
            if quickrig.name_index != 0:
                name += str(index)
                index += 1
                print( name)
            if quickrig.side != 'None':
                name += quickrig.side
                print( name)
            pb.name = name
        return {'FINISHED'}
    

class QuickRigSetRotMode( bpy.types.Operator):
    bl_label = "Set Rotation Mode"
    bl_description = ""
    bl_idname = "quickrig.set_rot_mode"
    
    def execute( self, context):
        pbones = context.selected_pose_bones
        scene = context.scene
        quickrig = scene.quickrig
        
        for pb in pbones: 
            pb.rotation_mode = quickrig.rotation_mode
        return {'FINISHED'}
    
    
class QuickRigRenameMirrored( bpy.types.Operator):
    bl_label = "Rename Mirrored"
    bl_idname = "quickrig.rename_mirrored"
    
    def execute( self, context):
        edit_bones = context.object.data.edit_bones
        sb = [ bone for bone in edit_bones if bone.select]
        scene = context.scene
        quickrig = scene.quickrig
        
        for b in sb:
            for bone in edit_bones:
                if bone.head == Vector(( -b.head[0], b.head[1], b.head[2])) and bone.tail == Vector(( -b.tail[0], b.tail[1], b.tail[2])):
                    if b.name.endswith( ".L"):
                        bone.name = b.name[:-2] + ".R"
                    if b.name.endswith( ".R"):
                        bone.name = b.name[:-2] + ".L"
                    if b.name.endswith( ".l"):
                        bone.name = b.name[:-2] + ".r"
                    if b.name.endswith( ".r"):
                        bone.name = b.name[:-2] + ".l"
        
        return {'FINISHED'}


class QuickRigCreateParent( bpy.types.Operator):
    bl_label = "Create MCH Parent"
    bl_idname = "quickrig.create_parent"
    bl_description = "Create an empty parent bone over the current bone"
    
    def execute( self, context):
        quickrig = context.scene.quickrig
        eb = context.object.data.edit_bones
        sb = [ b for b in eb if b.select]
        parent_name = quickrig.mch_name if quickrig.mch_name != "" else "MCH-parent_"
        for bone in sb:
            newbone = eb.new( parent_name + (bone.name.replace( "DEF-", "")).replace("CTRL-", ""))
            newbone.head = bone.head
            newbone.tail = bone.tail
            newbone.roll = bone.roll
            newbone.use_connect = bone.use_connect
            newbone.parent = bone.parent
            bone.use_connect = False
            bone.parent = newbone
            newbone.use_deform = False
        return {'FINISHED'}


class QuickRigCreateAction( bpy.types.Operator):
    bl_label = "Create Action"
    bl_idname = "quickrig.create_action"
    bl_description = "Create an action for the current selected bone positions"
    
    def execute( self, context):
        if context.object is None or context.selected_pose_bones == []:
            return {'CANCELLED'}
        scene = context.scene
        quickrig = scene.quickrig
        action_name = quickrig.action_name
        action = bpy.data.actions.new( action_name)
        context.object.animation_data.action = action
        action.use_fake_user = True
        current_frame = scene.frame_current
        scene.frame_set( frame = 1)
        pose_bones = {b:[0, 0, 0] for b in context.selected_pose_bones}
        # Set first keys.
        for pb in pose_bones.keys():
            if quickrig.action_loc:
                pose_bones[ pb][0] = pb.location.copy()
                pb.location = Vector((0, 0, 0))
                pb.keyframe_insert( data_path = 'location', group = "Location")
                
            if quickrig.action_rot:
                if pb.rotation_mode == 'QUATERNION':
                    rot = 'rotation_quaternion'
                    pose_bones[ pb][1] = pb.rotation_quaternion.copy()
                    pb.rotation_quaternion = Quaternion((1, 0, 0, 0))
                elif pb.rotation_mode == 'AXIS_ANGLE':
                    rot = 'rotation_axis_angle'
                    pose_bones[ pb][1] = pb.rotation_axis_angle[:]
                    pb.rotation_axis_angle = ( 0, 0, 1, 0)
                else:
                    rot = 'rotation_euler'
                    pose_bones[ pb][1] = pb.rotation_euler.copy()
                    pb.rotation_euler = Euler((0, 0, 0), pb.rotation_mode)
                    
                pb.keyframe_insert( data_path = rot, group = "Rotation")
                
            if quickrig.action_scale:
                pose_bones[ pb][2] = pb.scale.copy()
                pb.scale = Vector((0, 0, 0))
                pb.keyframe_insert( data_path = 'scale', group = "Scale")
                
        scene.frame_set( frame = 11)
        # Set last keys.
        for pb in pose_bones.keys():
            if quickrig.action_loc:
                pb.location = pose_bones[ pb][0]
                pb.keyframe_insert( data_path = 'location', group = "Location")
            if quickrig.action_rot:
                rot = 'rotation_euler'
                if pb.rotation_mode == 'QUATERNION':
                    rot = 'rotation_quaternion'
                if pb.rotation_mode == 'AXIS_ANGLE':
                    rot = 'rotation_axis_angle'
                pb.keyframe_insert( data_path = rot, group = "Rotation")
            if quickrig.action_scale:
                pb.keyframe_insert( data_path = 'scale', group = "Scale")
                
            if quickrig.action_rot:
                if pb.rotation_mode == 'QUATERNION':
                    rot = 'rotation_quaternion'
                    pb.rotation_quaternion = pose_bones[ pb][1]
                elif pb.rotation_mode == 'AXIS_ANGLE':
                    rot = 'rotation_axis_angle'
                    pb.rotation_axis_angle = pose_bones[ pb][1]
                else:
                    rot = 'rotation_euler'
                    pb.rotation_euler = pose_bones[ pb][1]
                    
                pb.keyframe_insert( data_path = rot, group = "Rotation")
                
            if quickrig.action_scale:
                pb.scale = pose_bones[ pb][2]
                pb.keyframe_insert( data_path = 'scale', group = "Scale")
                
        # Reset the action.
        context.object.animation_data.action = None
        scene.frame_set( frame = 1)
        for pb in pose_bones.keys():
            constr = pb.constraints.new( 'ACTION')
            constr.name = action_name
            constr.action = action
            constr.target_space = 'LOCAL'
            constr.target = context.object
            constr.frame_start = 1
            constr.frame_end = 11
        self.report( {'INFO'},"Success! Action created.")
        return {'FINISHED'}

def replace_mirror( name):
    if name[-2:] == ".L":
        new_name = name[:-1] + "R"
    elif name[-2:] == ".R":
        new_name = name[:-1] + "L"
    elif name[-2:] == ".l":
        new_name = name[:-1] + "r"
    elif name[-2:] == ".r":
        new_name = name[:-1] + "l"
    return new_name
    
class QuickRigMirrorConstraints( bpy.types.Operator):
    bl_label = "Mirror Constraints"
    bl_idname = "quickrig.mirror_constraints"
    bl_description = "Add mirrored constraints to mirrored bones"
    
    def execute( self, context):
        ob = context.object
        mode = ob.mode
        if mode != 'EDIT':
            bpy.ops.object.mode_set( mode = 'EDIT')
        edit_bones = ob.data.edit_bones
        sb = { bone.name:"" for bone in edit_bones if bone.select}
        scene = context.scene
        quickrig = scene.quickrig
        
        for b_name in sb.keys():
            print( b_name)
            b = edit_bones[ b_name]
            for bone in edit_bones:
                if bone.head == Vector(( -b.head[0], b.head[1], b.head[2])) and bone.tail == Vector(( -b.tail[0], b.tail[1], b.tail[2])):
                    sb[ b_name] = bone.name
        # Set to pose mode and add constraints.
        bpy.ops.object.mode_set( mode = 'POSE')
        pose_bones = ob.pose.bones
        for pb_name in sb.keys():
            pb = pose_bones[ pb_name]
            mir_pb = pose_bones[ sb[ pb_name]]
            for constr in pb.constraints:
                if constr:
                    mir_constr = mir_pb.constraints.new( constr.type)
                    mir_constr.name = constr.name
                    if constr.type == 'COPY_LOCATION':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.use_x = constr.use_x
                        mir_constr.use_y = constr.use_y
                        mir_constr.use_z = constr.use_z
                        mir_constr.invert_x = constr.invert_x
                        mir_constr.invert_y = constr.invert_y
                        mir_constr.invert_z = constr.invert_z
                        mir_constr.use_offset = constr.use_offset
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'COPY_ROTATION':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.use_x = constr.use_x
                        mir_constr.use_y = constr.use_y
                        mir_constr.use_z = constr.use_z
                        mir_constr.invert_x = constr.invert_x
                        mir_constr.invert_y = constr.invert_y
                        mir_constr.invert_z = constr.invert_z
                        mir_constr.use_offset = constr.use_offset
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'COPY_SCALE':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.use_x = constr.use_x
                        mir_constr.use_y = constr.use_y
                        mir_constr.use_z = constr.use_z
                        mir_constr.use_offset = constr.use_offset
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'COPY_TRANSFORMS':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'LIMIT_DISTANCE':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.distance = constr.distance
                        mir_constr.limit_mode = constr.limit_mode
                        mir_constr.use_transform_limit = constr.use_transform_limit
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'LIMIT_LOCATION':
                        mir_constr.use_min_x = constr.use_min_x
                        mir_constr.use_min_y = constr.use_min_y
                        mir_constr.use_min_z = constr.use_min_z
                        mir_constr.min_x = constr.min_x
                        mir_constr.min_y = constr.min_y
                        mir_constr.min_z = constr.min_z
                        mir_constr.use_max_x = constr.use_max_x
                        mir_constr.use_max_y = constr.use_max_y
                        mir_constr.use_max_z = constr.use_max_z
                        mir_constr.max_x = constr.max_x
                        mir_constr.max_y = constr.max_y
                        mir_constr.max_z = constr.max_z
                        mir_constr.use_transform_limit = constr.use_transform_limit
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'LIMIT_ROTATION':
                        mir_constr.use_limit_x = constr.use_limit_x
                        mir_constr.use_limit_y = constr.use_limit_y
                        mir_constr.use_limit_z = constr.use_limit_z
                        mir_constr.min_x = constr.min_x
                        mir_constr.min_y = constr.min_y
                        mir_constr.min_z = constr.min_z
                        mir_constr.max_x = constr.max_x
                        mir_constr.max_y = constr.max_y
                        mir_constr.max_z = constr.max_z
                        mir_constr.use_transform_limit = constr.use_transform_limit
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'LIMIT_SCALE':
                        mir_constr.use_min_x = constr.use_min_x
                        mir_constr.use_min_y = constr.use_min_y
                        mir_constr.use_min_z = constr.use_min_z
                        mir_constr.min_x = constr.min_x
                        mir_constr.min_y = constr.min_y
                        mir_constr.min_z = constr.min_z
                        mir_constr.use_max_x = constr.use_max_x
                        mir_constr.use_max_y = constr.use_max_y
                        mir_constr.use_max_z = constr.use_max_z
                        mir_constr.max_x = constr.max_x
                        mir_constr.max_y = constr.max_y
                        mir_constr.max_z = constr.max_z
                        mir_constr.use_transform_limit = constr.use_transform_limit
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'MAINTAIN_VOLUME':
                        mir_constr.free_axis = constr.free_axis
                        mir_constr.volume = constr.volume
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'TRANSFORM':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.use_motion_extrapolate = constr.use_motion_extrapolate
                        mir_constr.map_from = constr.map_from
                        mir_constr.from_min_x = constr.from_min_x
                        mir_constr.from_min_y = constr.from_min_y
                        mir_constr.from_min_z = constr.from_min_z
                        mir_constr.from_max_x = constr.from_max_x
                        mir_constr.from_max_y = constr.from_max_y
                        mir_constr.from_max_z = constr.from_max_z
                        mir_constr.from_min_x_rot = constr.from_min_x_rot
                        mir_constr.from_min_y_rot = constr.from_min_y_rot
                        mir_constr.from_min_z_rot = constr.from_min_z_rot
                        mir_constr.from_max_x_rot = constr.from_max_x_rot
                        mir_constr.from_max_y_rot = constr.from_max_y_rot
                        mir_constr.from_max_z_rot = constr.from_max_z_rot
                        mir_constr.from_min_x_scale = constr.from_min_x_scale
                        mir_constr.from_min_y_scale = constr.from_min_y_scale
                        mir_constr.from_min_z_scale = constr.from_min_z_scale
                        mir_constr.from_max_x_scale = constr.from_max_x_scale
                        mir_constr.from_max_y_scale = constr.from_max_y_scale
                        mir_constr.from_max_z_scale = constr.from_max_z_scale
                        mir_constr.map_to_x_from = constr.map_to_x_from
                        mir_constr.map_to_y_from = constr.map_to_y_from
                        mir_constr.map_to_z_from = constr.map_to_z_from
                        mir_constr.map_to = constr.map_to
                        mir_constr.to_min_x = constr.to_min_x
                        mir_constr.to_min_y = constr.to_min_y
                        mir_constr.to_min_z = constr.to_min_z
                        mir_constr.to_max_x = constr.to_max_x
                        mir_constr.to_max_y = constr.to_max_y
                        mir_constr.to_max_z = constr.to_max_z
                        mir_constr.to_min_x_rot = constr.to_min_x_rot
                        mir_constr.to_min_y_rot = constr.to_min_y_rot
                        mir_constr.to_min_z_rot = constr.to_min_z_rot
                        mir_constr.to_max_x_rot = constr.to_max_x_rot
                        mir_constr.to_max_y_rot = constr.to_max_y_rot
                        mir_constr.to_max_z_rot = constr.to_max_z_rot
                        mir_constr.to_min_x_scale = constr.to_min_x_scale
                        mir_constr.to_min_y_scale = constr.to_min_y_scale
                        mir_constr.to_min_z_scale = constr.to_min_z_scale
                        mir_constr.to_max_x_scale = constr.to_max_x_scale
                        mir_constr.to_max_y_scale = constr.to_max_y_scale
                        mir_constr.to_max_z_scale = constr.to_max_z_scale
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
                    if constr.type == 'DAMPED_TRACK':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.track_axis = constr.track_axis
                        mir_constr.influence = constr.influence
                    if constr.type == 'IK':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        if constr.pole_target != "":
                            mir_constr.pole_target = constr.pole_target
                            mir_constr.pole_subtarget = replace_mirror( constr.pole_subtarget)
                            mir_constr.pole_angle = constr.pole_angle
                        mir_constr.iterations = constr.iterations
                        mir_constr.chain_count = constr.chain_count
                        mir_constr.use_tail = constr.use_tail
                        mir_constr.use_stretch = constr.use_stretch
                        mir_constr.use_location = constr.use_location
                        mir_constr.weight = constr.weight
                        mir_constr.use_rotation = constr.use_rotation
                        mir_constr.orient_weight = constr.orient_weight
                        mir_constr.influence = constr.influence
                    if constr.type == 'LOCKED_TRACK':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.track_axis = constr.track_axis
                        mir_constr.lock_axis = constr.lock_axis
                        mir_constr.influence = constr.influence
                    if constr.type == 'ACTION':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.transform_channel = constr.transform_channel
                        mir_constr.target_space = constr.target_space
                        # Don't add the action - no way to specify?
                        mir_constr.use_bone_object_action = constr.use_bone_object_action
                        mir_constr.min = constr.min
                        mir_constr.max = constr.max
                        mir_constr.frame_start = constr.frame_start
                        mir_constr.frame_end = constr.frame_end
                        mir_constr.influence = constr.influence
                    if constr.type == 'STRETCH_TO':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.rest_length = constr.rest_length
                        mir_constr.bulge = constr.bulge
                        mir_constr.volume = constr.volume
                        mir_constr.keep_axis = constr.keep_axis
                        mir_constr.influence = constr.influence
                    if constr.type == 'TRACK_TO':
                        mir_constr.target = constr.target
                        mir_constr.subtarget = replace_mirror( constr.subtarget)
                        mir_constr.head_tail = constr.head_tail
                        mir_constr.track_axis = constr.track_axis
                        mir_constr.up_axis = constr.up_axis
                        mir_constr.use_target_z = constr.use_target_z
                        mir_constr.target_space = constr.target_space
                        mir_constr.owner_space = constr.owner_space
                        mir_constr.influence = constr.influence
        bpy.ops.object.mode_set( mode = mode)
        return {'FINISHED'}


class QuickRigDuplicateConstraints( bpy.types.Operator):
    bl_label = "Duplicate Constraint"
    bl_idname = "quickrig.duplicate_constraint"
    bl_description = "Duplicate constraint at the given index"
    
    def execute( self, context):
        scene = context.scene
        quickrig = scene.quickrig
        pb = context.active_pose_bone

        if len( pb.constraints) < quickrig.index:
            self.report( {'ERROR'}, "Index out of range.")
            return {'CANCELLED'}
            
        constr = pb.constraints[ quickrig.index - 1]
        dup_constr = pb.constraints.new( constr.type)
        dup_constr.name = dup_constr.name.replace( ".00", " ")
        dup_constr.name = dup_constr.name[:-1] + str( int( dup_constr.name[ -1]) + 1)
        if constr.type == 'COPY_LOCATION':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.use_x = constr.use_x
            dup_constr.use_y = constr.use_y
            dup_constr.use_z = constr.use_z
            dup_constr.invert_x = constr.invert_x
            dup_constr.invert_y = constr.invert_y
            dup_constr.invert_z = constr.invert_z
            dup_constr.use_offset = constr.use_offset
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'COPY_ROTATION':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.use_x = constr.use_x
            dup_constr.use_y = constr.use_y
            dup_constr.use_z = constr.use_z
            dup_constr.invert_x = constr.invert_x
            dup_constr.invert_y = constr.invert_y
            dup_constr.invert_z = constr.invert_z
            dup_constr.use_offset = constr.use_offset
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'COPY_SCALE':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.use_x = constr.use_x
            dup_constr.use_y = constr.use_y
            dup_constr.use_z = constr.use_z
            dup_constr.use_offset = constr.use_offset
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'COPY_TRANSFORMS':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'LIMIT_DISTANCE':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.distance = constr.distance
            dup_constr.limit_mode = constr.limit_mode
            dup_constr.use_transform_limit = constr.use_transform_limit
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'LIMIT_LOCATION':
            dup_constr.use_min_x = constr.use_min_x
            dup_constr.use_min_y = constr.use_min_y
            dup_constr.use_min_z = constr.use_min_z
            dup_constr.min_x = constr.min_x
            dup_constr.min_y = constr.min_y
            dup_constr.min_z = constr.min_z
            dup_constr.use_max_x = constr.use_max_x
            dup_constr.use_max_y = constr.use_max_y
            dup_constr.use_max_z = constr.use_max_z
            dup_constr.max_x = constr.max_x
            dup_constr.max_y = constr.max_y
            dup_constr.max_z = constr.max_z
            dup_constr.use_transform_limit = constr.use_transform_limit
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'LIMIT_ROTATION':
            dup_constr.use_limit_x = constr.use_limit_x
            dup_constr.use_limit_y = constr.use_limit_y
            dup_constr.use_limit_z = constr.use_limit_z
            dup_constr.min_x = constr.min_x
            dup_constr.min_y = constr.min_y
            dup_constr.min_z = constr.min_z
            dup_constr.max_x = constr.max_x
            dup_constr.max_y = constr.max_y
            dup_constr.max_z = constr.max_z
            dup_constr.use_transform_limit = constr.use_transform_limit
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'LIMIT_SCALE':
            dup_constr.use_min_x = constr.use_min_x
            dup_constr.use_min_y = constr.use_min_y
            dup_constr.use_min_z = constr.use_min_z
            dup_constr.min_x = constr.min_x
            dup_constr.min_y = constr.min_y
            dup_constr.min_z = constr.min_z
            dup_constr.use_max_x = constr.use_max_x
            dup_constr.use_max_y = constr.use_max_y
            dup_constr.use_max_z = constr.use_max_z
            dup_constr.max_x = constr.max_x
            dup_constr.max_y = constr.max_y
            dup_constr.max_z = constr.max_z
            dup_constr.use_transform_limit = constr.use_transform_limit
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'MAINTAIN_VOLUME':
            dup_constr.free_axis = constr.free_axis
            dup_constr.volume = constr.volume
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'TRANSFORM':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.use_motion_extrapolate = constr.use_motion_extrapolate
            dup_constr.map_from = constr.map_from
            dup_constr.from_min_x = constr.from_min_x
            dup_constr.from_min_y = constr.from_min_y
            dup_constr.from_min_z = constr.from_min_z
            dup_constr.from_max_x = constr.from_max_x
            dup_constr.from_max_y = constr.from_max_y
            dup_constr.from_max_z = constr.from_max_z
            dup_constr.from_min_x_rot = constr.from_min_x_rot
            dup_constr.from_min_y_rot = constr.from_min_y_rot
            dup_constr.from_min_z_rot = constr.from_min_z_rot
            dup_constr.from_max_x_rot = constr.from_max_x_rot
            dup_constr.from_max_y_rot = constr.from_max_y_rot
            dup_constr.from_max_z_rot = constr.from_max_z_rot
            dup_constr.from_min_x_scale = constr.from_min_x_scale
            dup_constr.from_min_y_scale = constr.from_min_y_scale
            dup_constr.from_min_z_scale = constr.from_min_z_scale
            dup_constr.from_max_x_scale = constr.from_max_x_scale
            dup_constr.from_max_y_scale = constr.from_max_y_scale
            dup_constr.from_max_z_scale = constr.from_max_z_scale
            dup_constr.map_to_x_from = constr.map_to_x_from
            dup_constr.map_to_y_from = constr.map_to_y_from
            dup_constr.map_to_z_from = constr.map_to_z_from
            dup_constr.map_to = constr.map_to
            dup_constr.to_min_x = constr.to_min_x
            dup_constr.to_min_y = constr.to_min_y
            dup_constr.to_min_z = constr.to_min_z
            dup_constr.to_max_x = constr.to_max_x
            dup_constr.to_max_y = constr.to_max_y
            dup_constr.to_max_z = constr.to_max_z
            dup_constr.to_min_x_rot = constr.to_min_x_rot
            dup_constr.to_min_y_rot = constr.to_min_y_rot
            dup_constr.to_min_z_rot = constr.to_min_z_rot
            dup_constr.to_max_x_rot = constr.to_max_x_rot
            dup_constr.to_max_y_rot = constr.to_max_y_rot
            dup_constr.to_max_z_rot = constr.to_max_z_rot
            dup_constr.to_min_x_scale = constr.to_min_x_scale
            dup_constr.to_min_y_scale = constr.to_min_y_scale
            dup_constr.to_min_z_scale = constr.to_min_z_scale
            dup_constr.to_max_x_scale = constr.to_max_x_scale
            dup_constr.to_max_y_scale = constr.to_max_y_scale
            dup_constr.to_max_z_scale = constr.to_max_z_scale
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
        if constr.type == 'DAMPED_TRACK':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.track_axis = constr.track_axis
            dup_constr.influence = constr.influence
        if constr.type == 'IK':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            if constr.pole_target != "":
                dup_constr.pole_target = constr.pole_target
                dup_constr.pole_subtarget = constr.pole_subtarget
                dup_constr.pole_angle = constr.pole_angle
            dup_constr.iterations = constr.iterations
            dup_constr.chain_count = constr.chain_count
            dup_constr.use_tail = constr.use_tail
            dup_constr.use_stretch = constr.use_stretch
            dup_constr.use_location = constr.use_location
            dup_constr.weight = constr.weight
            dup_constr.use_rotation = constr.use_rotation
            dup_constr.orient_weight = constr.orient_weight
            dup_constr.influence = constr.influence
        if constr.type == 'LOCKED_TRACK':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.track_axis = constr.track_axis
            dup_constr.lock_axis = constr.lock_axis
            dup_constr.influence = constr.influence
        if constr.type == 'ACTION':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.transform_channel = constr.transform_channel
            dup_constr.target_space = constr.target_space
            # Don't add the action - no way to specify?
            dup_constr.use_bone_object_action = constr.use_bone_object_action
            dup_constr.min = constr.min
            dup_constr.max = constr.max
            dup_constr.frame_start = constr.frame_start
            dup_constr.frame_end = constr.frame_end
            dup_constr.influence = constr.influence
        if constr.type == 'STRETCH_TO':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.rest_length = constr.rest_length
            dup_constr.bulge = constr.bulge
            dup_constr.volume = constr.volume
            dup_constr.keep_axis = constr.keep_axis
            dup_constr.influence = constr.influence
        if constr.type == 'TRACK_TO':
            dup_constr.target = constr.target
            dup_constr.subtarget = constr.subtarget
            dup_constr.head_tail = constr.head_tail
            dup_constr.track_axis = constr.track_axis
            dup_constr.up_axis = constr.up_axis
            dup_constr.use_target_z = constr.use_target_z
            dup_constr.target_space = constr.target_space
            dup_constr.owner_space = constr.owner_space
            dup_constr.influence = constr.influence
            
        return { 'FINISHED'}



        
#-----------------------------
def create_hips( scene):
    mesh = bpy.data.meshes.new( "WGT-hips.mesh")
    faces = []
    verts = [(0.0, 1.0, 0.0), (-0.19509032368659973, 0.9807852506637573, 0.0), (-0.3826834559440613, 0.9238795042037964, 0.0), (-0.5555702447891235, 0.8314695954322815, 0.0), (-0.7071067690849304, 0.7071067690849304, 0.0), (-0.8314696550369263, 0.5555701851844788, 0.0), (-0.9238795042037964, 0.3826834261417389, 0.0), (-0.9807852506637573, 0.19509035348892212, 0.0), (-1.0, 7.549790126404332e-08, 0.0), (-0.9807853102684021, -0.19509020447731018, 0.0), (-0.9238795638084412, -0.38268327713012695, 0.0), (-0.8314696550369263, -0.5555701851844788, 0.0), (-0.7071067690849304, -0.7071067690849304, 0.0), (-0.5555701851844788, -0.8314696550369263, 0.0), (-0.38268327713012695, -0.9238796234130859, 0.0), (-0.19509008526802063, -0.9807853102684021, 0.0), (3.2584136988589307e-07, -1.0, 0.0), (0.19509072601795197, -0.9807851910591125, 0.0), (0.3826838731765747, -0.9238793253898621, 0.0), (0.5555707216262817, -0.8314692974090576, 0.0), (0.7071072459220886, -0.707106351852417, 0.0), (0.8314700126647949, -0.5555696487426758, 0.0), (0.923879861831665, -0.3826826810836792, 0.0), (0.9807854294776917, -0.1950894594192505, 0.0), (1.0, 9.655991561885457e-07, 0.0), (0.980785071849823, 0.1950913518667221, 0.0), (0.923879086971283, 0.38268446922302246, 0.0), (0.831468939781189, 0.5555712580680847, 0.0), (0.7071058750152588, 0.707107663154602, 0.0), (0.5555691123008728, 0.8314703702926636, 0.0), (0.38268208503723145, 0.9238801002502441, 0.0), (0.19508881866931915, 0.9807855486869812, 0.0), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), (0, 16), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-hips", mesh)
    scene.objects.link( obj)
    

def create_torso( scene):
    mesh = bpy.data.meshes.new( "WGT-torso.mesh")
    faces = []
    verts = [(0.0, 1.0, 0.0), (-0.19509032368659973, 0.9807852506637573, 0.0), (-0.3826834559440613, 0.9238795042037964, 0.0), (-0.5555702447891235, 0.8314695954322815, 0.0), (-0.7071067690849304, 0.7071067690849304, 0.0), (-0.8314696550369263, 0.5555701851844788, 0.0), (-0.9238795042037964, 0.3826834261417389, 0.0), (-0.9807852506637573, 0.19509035348892212, 0.0), (-1.0, 7.549790126404332e-08, 0.0), (-0.9807853102684021, -0.19509020447731018, 0.0), (-0.9238795638084412, -0.38268327713012695, 0.0), (-0.8314696550369263, -0.5555701851844788, 0.0), (-0.7071067690849304, -0.7071067690849304, 0.0), (-0.5555701851844788, -0.8314696550369263, 0.0), (-0.38268327713012695, -0.9238796234130859, 0.0), (-0.19509008526802063, -0.9807853102684021, 0.0), (3.2584136988589307e-07, -1.0, 0.0), (0.19509072601795197, -0.9807851910591125, 0.0), (0.3826838731765747, -0.9238793253898621, 0.0), (0.5555707216262817, -0.8314692974090576, 0.0), (0.7071072459220886, -0.707106351852417, 0.0), (0.8314700126647949, -0.5555696487426758, 0.0), (0.923879861831665, -0.3826826810836792, 0.0), (0.9807854294776917, -0.1950894594192505, 0.0), (1.0, 9.655991561885457e-07, 0.0), (0.980785071849823, 0.1950913518667221, 0.0), (0.923879086971283, 0.38268446922302246, 0.0), (0.831468939781189, 0.5555712580680847, 0.0), (0.7071058750152588, 0.707107663154602, 0.0), (0.5555691123008728, 0.8314703702926636, 0.0), (0.38268208503723145, 0.9238801002502441, 0.0), (0.19508881866931915, 0.9807855486869812, 0.0), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-torso", mesh)
    scene.objects.link( obj)

def create_eye_track( scene):
    mesh = bpy.data.meshes.new( "WGT-eye_track.mesh")
    faces = []
    verts = [(1.9053419828414917, 5.960464477539063e-08, 1.0), (1.7102516889572144, 5.960464477539063e-08, 0.9807852506637573), (1.5226584672927856, 5.960464477539063e-08, 0.9238795042037964), (1.3497717380523682, 5.960464477539063e-08, 0.8314695954322815), (1.198235273361206, 5.960464477539063e-08, 0.7071067690849304), (1.0738723278045654, 5.960464477539063e-08, 0.5555701851844788), (0.9814624786376953, 2.9802322387695312e-08, 0.3826834261417389), (0.9245567321777344, 1.4901161193847656e-08, 0.19509035348892212), (0.9053419828414917, 7.105427357601002e-15, 7.549790126404332e-08), (0.9245566725730896, -1.4901161193847656e-08, -0.19509020447731018), (0.9814624190330505, -2.9802322387695312e-08, -0.38268327713012695), (1.0738723278045654, -5.960464477539063e-08, -0.5555701851844788), (1.198235273361206, -5.960464477539063e-08, -0.7071067690849304), (1.3497718572616577, -5.960464477539063e-08, -0.8314696550369263), (1.5226587057113647, -5.960464477539063e-08, -0.9238796234130859), (1.7102519273757935, -5.960464477539063e-08, -0.9807853102684021), (1.9053423404693604, -5.960464477539063e-08, -1.0), (2.100432872772217, -5.960464477539063e-08, -0.9807851910591125), (2.2880258560180664, -5.960464477539063e-08, -0.9238793253898621), (2.4609127044677734, -5.960464477539063e-08, -0.8314692974090576), (2.6124491691589355, -5.960464477539063e-08, -0.707106351852417), (2.736812114715576, -5.960464477539063e-08, -0.5555696487426758), (2.829221725463867, -2.9802322387695312e-08, -0.3826826810836792), (2.886127471923828, -1.4901161193847656e-08, -0.1950894594192505), (2.9053421020507812, 1.1368683772161603e-13, 9.655991561885457e-07), (2.88612699508667, 1.4901161193847656e-08, 0.1950913518667221), (2.829221248626709, 2.9802322387695312e-08, 0.38268446922302246), (2.7368111610412598, 5.960464477539063e-08, 0.5555712580680847), (2.612447738647461, 5.960464477539063e-08, 0.707107663154602), (2.460911273956299, 5.960464477539063e-08, 0.8314703702926636), (2.2880239486694336, 5.960464477539063e-08, 0.9238801002502441), (2.100430965423584, 5.960464477539063e-08, 0.9807855486869812), (1.9053421020507812, 1.1920928955078125e-07, 1.422334909439087), (1.627858281135559, 1.1920928955078125e-07, 1.3950051069259644), (1.3610379695892334, 1.1920928955078125e-07, 1.3140661716461182), (1.1151350736618042, 1.1920928955078125e-07, 1.2373007535934448), (-0.9814624786376953, 2.9802322387695312e-08, 0.3826834261417389), (-1.0738723278045654, 5.960464477539063e-08, 0.5555701851844788), (-1.198235273361206, 5.960464477539063e-08, 0.7071067690849304), (-1.3497717380523682, 5.960464477539063e-08, 0.8314695954322815), (-1.5226584672927856, 5.960464477539063e-08, 0.9238795042037964), (-1.7102516889572144, 5.960464477539063e-08, 0.9807852506637573), (-1.9053419828414917, 5.960464477539063e-08, 1.0), (0.0, 1.1920928955078125e-07, 1.1826282739639282), (1.1151351928710938, -1.1920928955078125e-07, -1.2333247661590576), (1.3610382080078125, -1.1920928955078125e-07, -1.3140664100646973), (1.6278586387634277, -1.1920928955078125e-07, -1.3950053453445435), (1.9053425788879395, -1.1920928955078125e-07, -1.422335147857666), (2.1828267574310303, -1.1920928955078125e-07, -1.395005226135254), (2.4496467113494873, -1.1920928955078125e-07, -1.3140660524368286), (2.695549726486206, -1.1920928955078125e-07, -1.1826279163360596), (2.911085367202759, -1.1920928955078125e-07, -1.0057421922683716), (3.0879712104797363, -5.960464477539063e-08, -0.7902063131332397), (3.2194085121154785, -5.960464477539063e-08, -0.5443031191825867), (3.3003478050231934, -2.9802322387695312e-08, -0.27748268842697144), (3.3276772499084473, 1.1368683772161603e-13, 1.2412465366651304e-06), (3.300346851348877, 2.9802322387695312e-08, 0.2774851322174072), (3.2194080352783203, 5.960464477539063e-08, 0.5443053841590881), (3.0879697799682617, 5.960464477539063e-08, 0.7902083396911621), (2.911083221435547, 1.1920928955078125e-07, 1.0057439804077148), (2.6955478191375732, 1.1920928955078125e-07, 1.1826293468475342), (2.449644088745117, 1.1920928955078125e-07, 1.314067006111145), (2.182823896408081, 1.1920928955078125e-07, 1.3950055837631226), (-0.9245567321777344, 1.4901161193847656e-08, 0.19509035348892212), (-0.9053419828414917, 7.105427357601002e-15, 7.549790126404332e-08), (-0.9245566725730896, -1.4901161193847656e-08, -0.19509020447731018), (-0.9814624190330505, -2.9802322387695312e-08, -0.38268327713012695), (-1.0738723278045654, -5.960464477539063e-08, -0.5555701851844788), (-1.198235273361206, -5.960464477539063e-08, -0.7071067690849304), (-1.3497718572616577, -5.960464477539063e-08, -0.8314696550369263), (-1.5226587057113647, -5.960464477539063e-08, -0.9238796234130859), (-1.7102519273757935, -5.960464477539063e-08, -0.9807853102684021), (-1.9053423404693604, -5.960464477539063e-08, -1.0), (-2.100432872772217, -5.960464477539063e-08, -0.9807851910591125), (-2.2880258560180664, -5.960464477539063e-08, -0.9238793253898621), (-2.4609127044677734, -5.960464477539063e-08, -0.8314692974090576), (-2.6124491691589355, -5.960464477539063e-08, -0.707106351852417), (-2.736812114715576, -5.960464477539063e-08, -0.5555696487426758), (-2.829221725463867, -2.9802322387695312e-08, -0.3826826810836792), (-2.886127471923828, -1.4901161193847656e-08, -0.1950894594192505), (-2.9053421020507812, 1.1368683772161603e-13, 9.655991561885457e-07), (-2.88612699508667, 1.4901161193847656e-08, 0.1950913518667221), (-2.829221248626709, 2.9802322387695312e-08, 0.38268446922302246), (-2.7368111610412598, 5.960464477539063e-08, 0.5555712580680847), (-2.612447738647461, 5.960464477539063e-08, 0.707107663154602), (-2.460911273956299, 5.960464477539063e-08, 0.8314703702926636), (-2.2880239486694336, 5.960464477539063e-08, 0.9238801002502441), (-2.100430965423584, 5.960464477539063e-08, 0.9807855486869812), (-1.9053421020507812, 1.1920928955078125e-07, 1.422334909439087), (-1.627858281135559, 1.1920928955078125e-07, 1.3950051069259644), (-1.3610379695892334, 1.1920928955078125e-07, 1.3140661716461182), (-1.1151350736618042, 1.1920928955078125e-07, 1.2373007535934448), (0.0, -1.1920928955078125e-07, -1.1826285123825073), (-1.1151351928710938, -1.1920928955078125e-07, -1.2333247661590576), (-1.3610382080078125, -1.1920928955078125e-07, -1.3140664100646973), (-1.6278586387634277, -1.1920928955078125e-07, -1.3950053453445435), (-1.9053425788879395, -1.1920928955078125e-07, -1.422335147857666), (-2.1828267574310303, -1.1920928955078125e-07, -1.395005226135254), (-2.4496467113494873, -1.1920928955078125e-07, -1.3140660524368286), (-2.695549726486206, -1.1920928955078125e-07, -1.1826279163360596), (-2.911085367202759, -1.1920928955078125e-07, -1.0057421922683716), (-3.0879712104797363, -5.960464477539063e-08, -0.7902063131332397), (-3.2194085121154785, -5.960464477539063e-08, -0.5443031191825867), (-3.3003478050231934, -2.9802322387695312e-08, -0.27748268842697144), (-3.3276772499084473, 1.1368683772161603e-13, 1.2412465366651304e-06), (-3.300346851348877, 2.9802322387695312e-08, 0.2774851322174072), (-3.2194080352783203, 5.960464477539063e-08, 0.5443053841590881), (-3.0879697799682617, 5.960464477539063e-08, 0.7902083396911621), (-2.911083221435547, 1.1920928955078125e-07, 1.0057439804077148), (-2.6955478191375732, 1.1920928955078125e-07, 1.1826293468475342), (-2.449644088745117, 1.1920928955078125e-07, 1.314067006111145), (-2.182823896408081, 1.1920928955078125e-07, 1.3950055837631226), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), (33, 32), (34, 33), (35, 34), (64, 63), (63, 36), (36, 37), (37, 38), (38, 39), (39, 40), (40, 41), (41, 42), (43, 35), (45, 44), (46, 45), (47, 46), (48, 47), (49, 48), (50, 49), (51, 50), (52, 51), (53, 52), (54, 53), (55, 54), (56, 55), (57, 56), (58, 57), (59, 58), (60, 59), (61, 60), (62, 61), (32, 62), (65, 64), (66, 65), (67, 66), (68, 67), (69, 68), (70, 69), (71, 70), (72, 71), (73, 72), (74, 73), (75, 74), (76, 75), (77, 76), (78, 77), (79, 78), (80, 79), (81, 80), (82, 81), (83, 82), (84, 83), (85, 84), (86, 85), (87, 86), (42, 87), (89, 88), (90, 89), (91, 90), (93, 92), (94, 93), (95, 94), (96, 95), (97, 96), (98, 97), (99, 98), (100, 99), (101, 100), (102, 101), (103, 102), (104, 103), (105, 104), (106, 105), (107, 106), (108, 107), (109, 108), (110, 109), (111, 110), (88, 111), (44, 92), (43, 91), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-eye_track", mesh)
    scene.objects.link( obj)

    
def create_root( scene):
    mesh = bpy.data.meshes.new( "WGT-root.mesh")
    verts = [(0.38268208503723145, 1.5297504663467407, 0.0), (0.9238792657852173, 0.38268399238586426, 0.0), (0.831468939781189, 0.5555712580680847, 0.0), (0.7071058750152588, 0.707107663154602, 0.0), (0.5555691123008728, 0.8314703702926636, 0.0), (0.38268208503723145, 0.9238801002502441, 0.0), (-0.3826834559440613, 1.529749870300293, 0.0), (0.7284911870956421, 1.5297504663467407, 0.0), (-0.7284924983978271, 1.529749870300293, 0.0), (-6.556510925292969e-07, 2.197329521179199, 0.0), (1.5297504663467407, -0.3826819658279419, 0.0), (-0.3826819658279419, -1.5297504663467407, 0.0), (0.5555713176727295, -0.8314688801765442, 0.0), (0.7071077227592468, -0.7071057558059692, 0.0), (0.8314704298973083, -0.555569052696228, 0.0), (0.9238801002502441, -0.38268208503723145, 0.0), (1.529749870300293, 0.38268351554870605, 0.0), (1.5297504663467407, -0.7284911870956421, 0.0), (1.529749870300293, 0.7284926176071167, 0.0), (2.197329521179199, 7.152557373046875e-07, 0.0), (0.38268357515335083, -0.9238795042037964, 0.0), (-0.9238792657852173, -0.3826841115951538, 0.0), (-0.831468939781189, -0.5555713772773743, 0.0), (-0.7071058750152588, -0.707107663154602, 0.0), (-0.5555689930915833, -0.8314703702926636, 0.0), (-0.3826819658279419, -0.9238801002502441, 0.0), (0.38268357515335083, -1.529749870300293, 0.0), (-0.7284910678863525, -1.5297507047653198, 0.0), (0.7284926176071167, -1.529749870300293, 0.0), (8.477477422275115e-07, -2.197329521179199, 0.0), (-1.5297504663467407, 0.38268184661865234, 0.0), (-0.3826846480369568, 0.9238791465759277, 0.0), (-0.5555713176727295, 0.8314687609672546, 0.0), (-0.7071078419685364, 0.7071057558059692, 0.0), (-0.8314705491065979, 0.555569052696228, 0.0), (-0.9238801002502441, 0.3826819658279419, 0.0), (-1.529749870300293, -0.3826836347579956, 0.0), (-1.5297507047653198, 0.7284910678863525, 0.0), (-1.529749870300293, -0.7284927368164062, 0.0), (-2.197329521179199, -9.073523870029021e-07, 0.0), ]
    edges = [(0, 5), (2, 1), (3, 2), (4, 3), (5, 4), (7, 0), (6, 8), (9, 7), (8, 9), (10, 15), (13, 12), (14, 13), (15, 14), (11, 25), (17, 10), (16, 18), (19, 17), (18, 19), (1, 16), (22, 21), (23, 22), (24, 23), (25, 24), (20, 26), (27, 11), (26, 28), (29, 27), (28, 29), (30, 35), (32, 31), (33, 32), (34, 33), (35, 34), (37, 30), (36, 38), (39, 37), (38, 39), (21, 36), (31, 6), (12, 20), ]
    faces = []
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-root", mesh)
    scene.objects.link( obj)

def create_fk_arms( scene):
    mesh = bpy.data.meshes.new( "WGT-fk_arm.mesh")
    faces = []
    verts = [(0.0, 1.5595731735229492, 1.0), (-0.19509032368659973, 1.5595731735229492, 0.9807852506637573), (-0.3826834559440613, 1.5595731735229492, 0.9238795042037964), (-0.5555702447891235, 1.5595731735229492, 0.8314695954322815), (-0.7071067690849304, 1.5595731735229492, 0.7071067690849304), (-0.8314696550369263, 1.5595731735229492, 0.5555701851844788), (-0.9238795042037964, 1.5595731735229492, 0.3826834261417389), (-0.9807852506637573, 1.5595731735229492, 0.19509035348892212), (-1.0, 1.5595731735229492, 7.549790126404332e-08), (-0.9807853102684021, 1.5595731735229492, -0.19509020447731018), (-0.9238795638084412, 1.5595731735229492, -0.38268327713012695), (-0.8314696550369263, 1.5595731735229492, -0.5555701851844788), (-0.7071067690849304, 1.5595731735229492, -0.7071067690849304), (-0.5555701851844788, 1.5595731735229492, -0.8314696550369263), (-0.38268327713012695, 1.5595731735229492, -0.9238796234130859), (-0.19509008526802063, 1.5595731735229492, -0.9807853102684021), (3.2584136988589307e-07, 1.5595731735229492, -1.0), (0.19509072601795197, 1.5595731735229492, -0.9807851910591125), (0.3826838731765747, 1.5595731735229492, -0.9238793253898621), (0.5555707216262817, 1.5595731735229492, -0.8314692974090576), (0.7071072459220886, 1.5595731735229492, -0.707106351852417), (0.8314700126647949, 1.5595731735229492, -0.5555696487426758), (0.923879861831665, 1.5595731735229492, -0.3826826810836792), (0.9807854294776917, 1.5595731735229492, -0.1950894594192505), (1.0, 1.5595731735229492, 9.655991561885457e-07), (0.980785071849823, 1.5595731735229492, 0.1950913518667221), (0.923879086971283, 1.5595731735229492, 0.38268446922302246), (0.831468939781189, 1.5595731735229492, 0.5555712580680847), (0.7071058750152588, 1.5595731735229492, 0.707107663154602), (0.5555691123008728, 1.5595731735229492, 0.8314703702926636), (0.38268208503723145, 1.5595731735229492, 0.9238801002502441), (0.19508881866931915, 1.5595731735229492, 0.9807855486869812), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), (0, 16), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-fk_arm", mesh)
    scene.objects.link( obj)

def create_fingers( scene):
    mesh = bpy.data.meshes.new( "WGT-finger.mesh")
    faces = []
    verts = [(-0.5033295154571533, 7.697004318237305, 0.0), (0.5033295154571533, 7.697004318237305, 0.0), (-0.5033295154571533, 8.703662872314453, 0.0), (0.5033295154571533, 8.703662872314453, 0.0), (0.0, 7.697004318237305, 0.0), (0.0, 0.0, 0.0), ]
    edges = [(2, 0), (4, 1), (1, 3), (3, 2), (0, 4), (4, 5), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-finger", mesh)
    scene.objects.link( obj)

def create_hands( scene):
    mesh = bpy.data.meshes.new( "WGT-hand.mesh")
    faces = []
    verts = [(-0.7414229512214661, 0.8643848896026611, 0.0), (-0.7298382520675659, 0.6017951965332031, 0.0), (-0.6950840353965759, 0.37421754002571106, 0.0), (0.18535573780536652, 0.02409803867340088, 0.0), (0.3591267466545105, 0.04160401597619057, 0.0), (0.5097283124923706, 0.09412194788455963, 0.0), (0.7414229512214661, 1.4245761632919312, 0.0), (0.7298382520675659, 1.6871658563613892, 0.0), (0.6950840353965759, 1.9147435426712036, 0.0), (-0.18535573780536652, 2.2648630142211914, 0.0), (-0.3591267466545105, 2.24735689163208, 0.0), (-0.5097283124923706, 2.1948390007019043, 0.0), (-0.6950840353965759, 1.914743423461914, 0.0), (-0.7298382520675659, 1.6871657371520996, 0.0), (-0.7414229512214661, 1.4245760440826416, 0.0), (-0.5097283124923706, 0.09412194043397903, 0.0), (-0.3591267466545105, 0.04160401225090027, 0.0), (-0.18535573780536652, 0.02409803867340088, 0.0), (0.6950840353965759, 0.37421756982803345, 0.0), (0.7298382520675659, 0.6017952561378479, 0.0), (0.7414229512214661, 0.8643848896026611, 0.0), (0.5097283124923706, 2.1948390007019043, 0.0), (0.3591267466545105, 2.24735689163208, 0.0), (0.18535573780536652, 2.2648630142211914, 0.0), (-0.6255756616592407, 0.19915780425071716, 0.0), (0.6255756616592407, 0.19915780425071716, 0.0), (-0.6255756616592407, 2.0898032188415527, 0.0), (0.6255756616592407, 2.0898032188415527, 0.0), (-0.7414229512214661, 1.1444804668426514, 0.0), (0.0, 0.02409803867340088, 0.0), (0.7414229512214661, 1.1444804668426514, 0.0), (0.0, 2.2648630142211914, 0.0), ]
    edges = [(28, 0), (0, 1), (1, 2), (2, 24), (29, 3), (3, 4), (4, 5), (5, 25), (30, 6), (6, 7), (7, 8), (8, 27), (31, 9), (9, 10), (10, 11), (11, 26), (26, 12), (12, 13), (13, 14), (14, 28), (24, 15), (15, 16), (16, 17), (17, 29), (25, 18), (18, 19), (19, 20), (20, 30), (27, 21), (21, 22), (22, 23), (23, 31), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-hand", mesh)
    scene.objects.link( obj)

def create_feet( scene):
    mesh = bpy.data.meshes.new( "WGT-feet.mesh")
    faces = []
    verts = [(-0.44452399015426636, 0.3856513500213623, 0.0), (-0.37411946058273315, 0.10514669120311737, 0.0), (0.0076272860169410706, -0.022357389330863953, 0.0), (0.21864920854568481, 0.048106849193573, 0.0), (-0.3041086196899414, 0.8815319538116455, 0.0), (-0.6155332326889038, 1.4213130474090576, 0.0), (-0.6194080114364624, 2.0389888286590576, 0.0), (-0.35235872864723206, 2.1902668476104736, 0.0), (0.08135256171226501, 2.1870241165161133, 0.0), (0.44603231549263, 1.8971235752105713, 0.0), (0.4745752513408661, 1.4505906105041504, 0.0), (0.3413235545158386, 0.3019739091396332, 0.0), (0.36810123920440674, 0.7381250858306885, 0.0), (-0.6476401686668396, 1.7760635614395142, 0.0), (-0.37581339478492737, 0.6397050619125366, 0.0), (-0.3316655158996582, 1.1395986080169678, 0.0), (0.4392724633216858, 1.0920895338058472, 0.0), (-0.6043012142181396, 2.939417600631714, 3.1626954388741524e-09), (-0.7533935904502869, 2.8297932147979736, 3.1626954388741524e-09), (-0.8071472644805908, 2.576949119567871, 3.1626954388741524e-09), (-0.7340739369392395, 2.3289973735809326, 3.1626954388741524e-09), (-0.5769790410995483, 2.2311854362487793, 3.1626954388741524e-09), (-0.4278866648674011, 2.3408095836639404, 3.1626954388741524e-09), (-0.37413305044174194, 2.593653678894043, 3.1626954388741524e-09), (-0.4472063183784485, 2.8416051864624023, 3.1626954388741524e-09), (0.10593394190073013, 2.757277727127075, 5.352253751311764e-09), (0.02013007551431656, 2.693203926086426, 5.352253751311764e-09), (-0.01541108638048172, 2.5385162830352783, 5.352253751311764e-09), (0.02013007551431656, 2.383828639984131, 5.352253751311764e-09), (0.10593394190073013, 2.3197548389434814, 5.352253751311764e-09), (0.1917378008365631, 2.383828639984131, 5.352253751311764e-09), (0.2272789180278778, 2.5385162830352783, 5.352253751311764e-09), (0.1917378008365631, 2.693203926086426, 5.352253751311764e-09), (0.3777464032173157, 2.596458673477173, 1.1677644629060069e-08), (0.3014982342720032, 2.5461392402648926, 1.1677644629060069e-08), (0.25990116596221924, 2.412181854248047, 1.1677644629060069e-08), (0.2773222029209137, 2.273057222366333, 1.1677644629060069e-08), (0.34355631470680237, 2.2102620601654053, 1.1677644629060069e-08), (0.4198044240474701, 2.2605814933776855, 1.1677644629060069e-08), (0.46140149235725403, 2.3945388793945312, 1.1677644629060069e-08), (0.44398045539855957, 2.533663511276245, 1.1677644629060069e-08), (0.5500245690345764, 2.3055102825164795, 2.4571711776388838e-08), (0.47796332836151123, 2.2720141410827637, 2.4571711776388838e-08), (0.43368178606033325, 2.1699278354644775, 2.4571711776388838e-08), (0.4431195855140686, 2.0590524673461914, 2.4571711776388838e-08), (0.5007481575012207, 2.0043373107910156, 2.4571711776388838e-08), (0.5728093385696411, 2.0378332138061523, 2.4571711776388838e-08), (0.6170908808708191, 2.1399195194244385, 2.4571711776388838e-08), (0.6076530814170837, 2.2507948875427246, 2.4571711776388838e-08), (-0.19920706748962402, 2.8730430603027344, 1.6543331149421192e-09), (-0.29302555322647095, 2.7982051372528076, 1.6543331149421192e-09), (-0.3318864703178406, 2.617530584335327, 1.6543331149421192e-09), (-0.29302555322647095, 2.4368560314178467, 1.6543331149421192e-09), (-0.19920706748962402, 2.36201810836792, 1.6543331149421192e-09), (-0.1053885892033577, 2.4368560314178467, 1.6543331149421192e-09), (-0.06652770936489105, 2.617530584335327, 1.6543331149421192e-09), (-0.1053885892033577, 2.7982051372528076, 1.6543331149421192e-09), ]
    edges = [(14, 4), (1, 0), (2, 1), (3, 2), (15, 5), (13, 6), (6, 7), (7, 8), (8, 9), (9, 10), (12, 11), (3, 11), (16, 12), (5, 13), (0, 14), (4, 15), (10, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (17, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (32, 31), (25, 32), (34, 33), (35, 34), (36, 35), (37, 36), (38, 37), (39, 38), (40, 39), (33, 40), (42, 41), (43, 42), (44, 43), (45, 44), (46, 45), (47, 46), (48, 47), (41, 48), (50, 49), (51, 50), (52, 51), (53, 52), (54, 53), (55, 54), (56, 55), (49, 56), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-feet", mesh)
    scene.objects.link( obj)

def create_head( scene):
    mesh = bpy.data.meshes.new( "WGT-head.mesh")
    faces = []
    verts = [(0.0, 1.5595731735229492, 1.0), (-0.19509032368659973, 1.5595731735229492, 0.9807852506637573), (-0.3826834559440613, 1.5595731735229492, 0.9238795042037964), (-0.5555702447891235, 1.5595731735229492, 0.8314695954322815), (-0.7071067690849304, 1.5595731735229492, 0.7071067690849304), (-0.8314696550369263, 1.5595731735229492, 0.5555701851844788), (-0.9238795042037964, 1.5595731735229492, 0.3826834261417389), (-0.9807852506637573, 1.5595731735229492, 0.19509035348892212), (-1.0, 1.5595731735229492, 7.549790126404332e-08), (-0.9807853102684021, 1.5595731735229492, -0.19509020447731018), (-0.9238795638084412, 1.5595731735229492, -0.38268327713012695), (-0.8314696550369263, 1.5595731735229492, -0.5555701851844788), (-0.7071067690849304, 1.5595731735229492, -0.7071067690849304), (-0.5555701851844788, 1.5595731735229492, -0.8314696550369263), (-0.38268327713012695, 1.5595731735229492, -0.9238796234130859), (-0.19509008526802063, 1.5595731735229492, -0.9807853102684021), (3.2584136988589307e-07, 1.5595731735229492, -1.0), (0.19509072601795197, 1.5595731735229492, -0.9807851910591125), (0.3826838731765747, 1.5595731735229492, -0.9238793253898621), (0.5555707216262817, 1.5595731735229492, -0.8314692974090576), (0.7071072459220886, 1.5595731735229492, -0.707106351852417), (0.8314700126647949, 1.5595731735229492, -0.5555696487426758), (0.923879861831665, 1.5595731735229492, -0.3826826810836792), (0.9807854294776917, 1.5595731735229492, -0.1950894594192505), (1.0, 1.5595731735229492, 9.655991561885457e-07), (0.980785071849823, 1.5595731735229492, 0.1950913518667221), (0.923879086971283, 1.5595731735229492, 0.38268446922302246), (0.831468939781189, 1.5595731735229492, 0.5555712580680847), (0.7071058750152588, 1.5595731735229492, 0.707107663154602), (0.5555691123008728, 1.5595731735229492, 0.8314703702926636), (0.38268208503723145, 1.5595731735229492, 0.9238801002502441), (0.19508881866931915, 1.5595731735229492, 0.9807855486869812), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-head", mesh)
    scene.objects.link( obj)

def create_ik_poles( scene):
    mesh = bpy.data.meshes.new( "WGT-ik_pole.mesh")
    faces = []
    verts = [(0.0, 0.0, -1.0), (0.0, 0.0, 1.0), (-1.0, 0.0, 0.0), (1.0, 0.0, 0.0), (0.0, 1.0, 0.0), (0.0, -1.0, 0.0), ]
    edges = [(2, 1), (0, 2), (3, 4), (2, 5), (5, 0), (4, 0), (3, 0), (5, 3), (1, 3), (1, 4), (1, 5), (4, 2), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-ik_pole", mesh)
    scene.objects.link( obj)

def create_chest( scene):
    mesh = bpy.data.meshes.new( "WGT-chest.mesh")
    faces = []
    verts = [(0.0, 5.960464477539063e-08, 1.0), (-0.19509032368659973, 5.960464477539063e-08, 0.9807852506637573), (-0.3826834559440613, 5.960464477539063e-08, 0.9238795042037964), (-0.5555702447891235, 5.960464477539063e-08, 0.8314695954322815), (-0.7071067690849304, 5.960464477539063e-08, 0.7071067690849304), (-0.8314696550369263, 5.960464477539063e-08, 0.5555701851844788), (-0.9238795042037964, 2.9802322387695312e-08, 0.3826834261417389), (-0.9807852506637573, 1.4901161193847656e-08, 0.19509035348892212), (-1.0, 7.105427357601002e-15, 7.549790126404332e-08), (-0.9807853102684021, -1.4901161193847656e-08, -0.19509020447731018), (-0.9238795638084412, -2.9802322387695312e-08, -0.38268327713012695), (-0.8314696550369263, -5.960464477539063e-08, -0.5555701851844788), (-0.7071067690849304, -5.960464477539063e-08, -0.7071067690849304), (-0.5555701851844788, -5.960464477539063e-08, -0.8314696550369263), (-0.38268327713012695, -5.960464477539063e-08, -0.9238796234130859), (-0.19509008526802063, -5.960464477539063e-08, -0.9807853102684021), (3.2584136988589307e-07, -5.960464477539063e-08, -1.0), (0.19509072601795197, -5.960464477539063e-08, -0.9807851910591125), (0.3826838731765747, -5.960464477539063e-08, -0.9238793253898621), (0.5555707216262817, -5.960464477539063e-08, -0.8314692974090576), (0.7071072459220886, -5.960464477539063e-08, -0.707106351852417), (0.8314700126647949, -5.960464477539063e-08, -0.5555696487426758), (0.923879861831665, -2.9802322387695312e-08, -0.3826826810836792), (0.9807854294776917, -1.4901161193847656e-08, -0.1950894594192505), (1.0, 1.1368683772161603e-13, 9.655991561885457e-07), (0.980785071849823, 1.4901161193847656e-08, 0.1950913518667221), (0.923879086971283, 2.9802322387695312e-08, 0.38268446922302246), (0.831468939781189, 5.960464477539063e-08, 0.5555712580680847), (0.7071058750152588, 5.960464477539063e-08, 0.707107663154602), (0.5555691123008728, 5.960464477539063e-08, 0.8314703702926636), (0.38268208503723145, 5.960464477539063e-08, 0.9238801002502441), (0.19508881866931915, 5.960464477539063e-08, 0.9807855486869812), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), (0, 16), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-chest", mesh)
    scene.objects.link( obj)

def create_face_ctrl( scene):
    mesh = bpy.data.meshes.new( "WGT-face_ctrl.mesh")
    faces = []
    verts = [(0.0, 5.960464477539063e-08, 1.0), (-0.19509032368659973, 5.960464477539063e-08, 0.9807852506637573), (-0.3826834559440613, 5.960464477539063e-08, 0.9238795042037964), (-0.5555702447891235, 5.960464477539063e-08, 0.8314695954322815), (-0.7071067690849304, 5.960464477539063e-08, 0.7071067690849304), (-0.8314696550369263, 5.960464477539063e-08, 0.5555701851844788), (-0.9238795042037964, 2.9802322387695312e-08, 0.3826834261417389), (-0.9807852506637573, 1.4901161193847656e-08, 0.19509035348892212), (-1.0, 7.105427357601002e-15, 7.549790126404332e-08), (-0.9807853102684021, -1.4901161193847656e-08, -0.19509020447731018), (-0.9238795638084412, -2.9802322387695312e-08, -0.38268327713012695), (-0.8314696550369263, -5.960464477539063e-08, -0.5555701851844788), (-0.7071067690849304, -5.960464477539063e-08, -0.7071067690849304), (-0.5555701851844788, -5.960464477539063e-08, -0.8314696550369263), (-0.38268327713012695, -5.960464477539063e-08, -0.9238796234130859), (-0.19509008526802063, -5.960464477539063e-08, -0.9807853102684021), (3.2584136988589307e-07, -5.960464477539063e-08, -1.0), (0.19509072601795197, -5.960464477539063e-08, -0.9807851910591125), (0.3826838731765747, -5.960464477539063e-08, -0.9238793253898621), (0.5555707216262817, -5.960464477539063e-08, -0.8314692974090576), (0.7071072459220886, -5.960464477539063e-08, -0.707106351852417), (0.8314700126647949, -5.960464477539063e-08, -0.5555696487426758), (0.923879861831665, -2.9802322387695312e-08, -0.3826826810836792), (0.9807854294776917, -1.4901161193847656e-08, -0.1950894594192505), (1.0, 1.1368683772161603e-13, 9.655991561885457e-07), (0.980785071849823, 1.4901161193847656e-08, 0.1950913518667221), (0.923879086971283, 2.9802322387695312e-08, 0.38268446922302246), (0.831468939781189, 5.960464477539063e-08, 0.5555712580680847), (0.7071058750152588, 5.960464477539063e-08, 0.707107663154602), (0.5555691123008728, 5.960464477539063e-08, 0.8314703702926636), (0.38268208503723145, 5.960464477539063e-08, 0.9238801002502441), (0.19508881866931915, 5.960464477539063e-08, 0.9807855486869812), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-ctrl", mesh)
    scene.objects.link( obj)

def create_plus( scene):
    mesh = bpy.data.meshes.new( "WGT-plus.mesh")
    faces = []
    verts = [(-1.0, -5.960464477539063e-08, -1.0), (1.0, -5.960464477539063e-08, -1.0), (-1.0, 5.960464477539063e-08, 1.0), (1.0, 5.960464477539063e-08, 1.0), (-1.0, 2.384185791015625e-07, 2.998868465423584), (1.0, 2.384185791015625e-07, 2.998868465423584), (3.0, -5.960464477539063e-08, -1.0), (3.0, 5.960464477539063e-08, 1.0), (-1.0, -2.384185791015625e-07, -3.0), (1.0, -2.384185791015625e-07, -3.0), (-3.0, -5.960464477539063e-08, -1.0), (-3.0, 5.960464477539063e-08, 1.0), ]
    edges = [(5, 4), (4, 2), (3, 5), (6, 7), (7, 3), (1, 6), (8, 9), (9, 1), (0, 8), (11, 10), (10, 0), (2, 11), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-plus", mesh)
    scene.objects.link( obj)

def create_palm_curl( scene):
    mesh = bpy.data.meshes.new( "WGT-palm_curl.mesh")
    faces = []
    verts = [(-0.40387529134750366, 0.8643848896026611, 0.5991041660308838), (-0.39756473898887634, 0.6017951965332031, 0.5991041660308838), (-0.3786330819129944, 0.37421751022338867, 0.5991041660308838), (0.10096882283687592, 0.02409803867340088, 0.5991041660308838), (0.1956270933151245, 0.041604042053222656, 0.5991041660308838), (0.27766427397727966, 0.09412193298339844, 0.5991041660308838), (0.40387529134750366, 1.4245761632919312, 0.5991041660308838), (0.39756473898887634, 1.6871658563613892, 0.5991041660308838), (0.3786330819129944, 1.9147435426712036, 0.5991041660308838), (-0.10096882283687592, 2.2648630142211914, 0.5991041660308838), (-0.1956270933151245, 2.24735689163208, 0.5991041660308838), (-0.27766427397727966, 2.1948390007019043, 0.5991041660308838), (-0.3786330819129944, 1.914743423461914, 0.5991041660308838), (-0.39756473898887634, 1.6871657371520996, 0.5991041660308838), (-0.40387529134750366, 1.4245760440826416, 0.5991041660308838), (-0.27766427397727966, 0.09412193298339844, 0.5991041660308838), (-0.1956270933151245, 0.041604042053222656, 0.5991041660308838), (-0.10096882283687592, 0.02409803867340088, 0.5991041660308838), (0.3786330819129944, 0.37421756982803345, 0.5991041660308838), (0.39756473898887634, 0.6017952561378479, 0.5991041660308838), (0.40387529134750366, 0.8643848896026611, 0.5991041660308838), (0.27766427397727966, 2.1948390007019043, 0.5991041660308838), (0.1956270933151245, 2.24735689163208, 0.5991041660308838), (0.10096882283687592, 2.2648630142211914, 0.5991041660308838), (-0.34076979756355286, 0.19915783405303955, 0.5991041660308838), (0.34076979756355286, 0.19915783405303955, 0.5991041660308838), (-0.34076979756355286, 2.0898032188415527, 0.5991041660308838), (0.34076979756355286, 2.0898032188415527, 0.5991041660308838), (-0.40387529134750366, 1.1444804668426514, 0.5991041660308838), (-8.480065538662984e-10, 0.02409803867340088, 0.5991041660308838), (0.40387529134750366, 1.1444804668426514, 0.5991041660308838), (-8.480065538662984e-10, 2.2648630142211914, 0.5991041660308838), (-0.40387529134750366, 0.8643848896026611, -0.3236270546913147), (-0.39756473898887634, 0.6017951965332031, -0.3236270546913147), (-0.3786330819129944, 0.37421751022338867, -0.3236270546913147), (0.10096882283687592, 0.02409803867340088, -0.3236270546913147), (0.1956270933151245, 0.041604042053222656, -0.3236270546913147), (0.27766427397727966, 0.09412193298339844, -0.3236270546913147), (0.40387529134750366, 1.4245761632919312, -0.3236270546913147), (0.39756473898887634, 1.6871658563613892, -0.3236270546913147), (0.3786330819129944, 1.9147435426712036, -0.3236270546913147), (-0.10096882283687592, 2.2648630142211914, -0.3236270546913147), (-0.1956270933151245, 2.24735689163208, -0.3236270546913147), (-0.27766427397727966, 2.1948390007019043, -0.3236270546913147), (-0.3786330819129944, 1.914743423461914, -0.3236270546913147), (-0.39756473898887634, 1.6871657371520996, -0.3236270546913147), (-0.40387529134750366, 1.4245760440826416, -0.3236270546913147), (-0.27766427397727966, 0.09412193298339844, -0.3236270546913147), (-0.1956270933151245, 0.041604042053222656, -0.3236270546913147), (-0.10096882283687592, 0.02409803867340088, -0.3236270546913147), (0.3786330819129944, 0.37421756982803345, -0.3236270546913147), (0.39756473898887634, 0.6017952561378479, -0.3236270546913147), (0.40387529134750366, 0.8643848896026611, -0.3236270546913147), (0.27766427397727966, 2.1948390007019043, -0.3236270546913147), (0.1956270933151245, 2.24735689163208, -0.3236270546913147), (0.10096882283687592, 2.2648630142211914, -0.3236270546913147), (-0.34076979756355286, 0.19915783405303955, -0.3236270546913147), (0.34076979756355286, 0.19915783405303955, -0.3236270546913147), (-0.34076979756355286, 2.0898032188415527, -0.3236270546913147), (0.34076979756355286, 2.0898032188415527, -0.3236270546913147), (-0.40387529134750366, 1.1444804668426514, -0.3236270546913147), (-8.480065538662984e-10, 0.02409803867340088, -0.3236270546913147), (0.40387529134750366, 1.1444804668426514, -0.3236270546913147), (-8.480065538662984e-10, 2.2648630142211914, -0.3236270546913147), ]
    edges = [(28, 0), (0, 1), (1, 2), (2, 24), (29, 3), (3, 4), (4, 5), (5, 25), (30, 6), (6, 7), (7, 8), (8, 27), (31, 9), (9, 10), (10, 11), (11, 26), (26, 12), (12, 13), (13, 14), (14, 28), (24, 15), (15, 16), (16, 17), (17, 29), (25, 18), (18, 19), (19, 20), (20, 30), (27, 21), (21, 22), (22, 23), (23, 31), (60, 32), (32, 33), (33, 34), (34, 56), (61, 35), (35, 36), (36, 37), (37, 57), (62, 38), (38, 39), (39, 40), (40, 59), (63, 41), (41, 42), (42, 43), (43, 58), (58, 44), (44, 45), (45, 46), (46, 60), (56, 47), (47, 48), (48, 49), (49, 61), (57, 50), (50, 51), (51, 52), (52, 62), (59, 53), (53, 54), (54, 55), (55, 63), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-palm_curl", mesh)
    scene.objects.link( obj)

def create_shoulder( scene):
    mesh = bpy.data.meshes.new( "WGT-shoulder.mesh")
    faces = []
    verts = [(-1.0, 0.011155128479003906, -1.0), (-0.40796077251434326, 8.325177192687988, -0.40796077251434326), (0.40796077251434326, 8.325177192687988, -0.40796077251434326), (1.0, 0.011155128479003906, -1.0), (-1.0, 0.011155128479003906, 1.0), (-0.40796077251434326, 8.325177192687988, 0.40796077251434326), (0.40796077251434326, 8.325177192687988, 0.40796077251434326), (1.0, 0.011155128479003906, 1.0), ]
    edges = [(0, 4), (4, 5), (5, 1), (1, 0), (5, 6), (6, 2), (2, 1), (6, 7), (7, 3), (3, 2), (7, 4), (0, 3), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-shoulder", mesh)
    scene.objects.link( obj)


def create_foot_roll( scene):
    mesh = bpy.data.meshes.new( "WGT-foot_roll.mesh")
    faces = []
    verts = [(-0.4999999701976776, 4.239184379577637, 0.0), (-0.6541239023208618, 4.21711540222168, 0.0), (-0.7831623554229736, 4.15090799331665, 0.0), (0.29589754343032837, 0.001923680305480957, 0.0), (0.3904916048049927, 0.02399274706840515, 0.0), (0.4770087003707886, 0.09019994735717773, 0.0), (-0.6938463449478149, 1.0612387657165527, 0.0), (-0.6557908058166504, 0.730202853679657, 0.0), (-0.6096583008766174, 0.44330498576164246, 0.0), (0.8979487419128418, 3.1798691749572754, 0.0), (0.9194231033325195, 3.5109052658081055, 0.0), (0.9158120155334473, 3.7978029251098633, 0.0), (0.7831623554229736, 4.15090799331665, 0.0), (0.6541239023208618, 4.21711540222168, 0.0), (0.4999999701976776, 4.239184379577637, 0.0), (0.1666666567325592, 4.239184379577637, 0.0), (0.0, 4.239184379577637, 0.0), (-0.1666666567325592, 4.239184379577637, 0.0), (-0.4770087003707886, 0.09019993990659714, 0.0), (-0.3904916048049927, 0.023992745205760002, 0.0), (-0.29589754343032837, 0.001923680305480957, 0.0), (-0.09863251447677612, 0.001923680305480957, 0.0), (0.0, 0.001923680305480957, 0.0), (0.09863251447677612, 0.001923680305480957, 0.0), (-0.9158119559288025, 3.7978029251098633, 0.0), (-0.9194231033325195, 3.5109052658081055, 0.0), (-0.8979487419128418, 3.1798691749572754, 0.0), (-0.8299145698547363, 2.473659038543701, 0.0), (-0.7958974838256836, 2.120553970336914, 0.0), (-0.7618803977966309, 1.767448902130127, 0.0), (0.6096583008766174, 0.44330501556396484, 0.0), (0.6557908058166504, 0.7302029132843018, 0.0), (0.6938463449478149, 1.0612388849258423, 0.0), (0.7618805170059204, 1.767448902130127, 0.0), (0.7958976030349731, 2.120553970336914, 0.0), (0.8299146890640259, 2.473659038543701, 0.0), (0.8745726346969604, 4.01849365234375, 0.0), (-0.8745725750923157, 4.01849365234375, 0.0), (0.551410436630249, 0.2226143479347229, 0.0), (-0.551410436630249, 0.2226143181324005, 0.0), (0.3333333134651184, 4.239184379577637, 0.0), (-0.3333333134651184, 4.239184379577637, 0.0), (-0.19726502895355225, 0.001923680305480957, 0.0), (0.19726502895355225, 0.001923680305480957, 0.0), (-0.8639316558837891, 2.8267641067504883, 0.0), (-0.7278633713722229, 1.4143438339233398, 0.0), (0.7278634309768677, 1.4143439531326294, 0.0), (0.8639317154884338, 2.8267641067504883, 0.0), ]
    edges = [(41, 0), (0, 1), (1, 2), (2, 37), (43, 3), (3, 4), (4, 5), (5, 38), (45, 6), (6, 7), (7, 8), (8, 39), (47, 9), (9, 10), (10, 11), (11, 36), (36, 12), (12, 13), (13, 14), (14, 40), (40, 15), (15, 16), (16, 17), (17, 41), (39, 18), (18, 19), (19, 20), (20, 42), (42, 21), (21, 22), (22, 23), (23, 43), (37, 24), (24, 25), (25, 26), (26, 44), (44, 27), (27, 28), (28, 29), (29, 45), (38, 30), (30, 31), (31, 32), (32, 46), (46, 33), (33, 34), (34, 35), (35, 47), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-foot_roll", mesh)
    scene.objects.link( obj)

def create_neck( scene):
    mesh = bpy.data.meshes.new( "WGT-neck.mesh")
    faces = []
    verts = [(5.453275520039824e-08, 0.9830835461616516, 1.6709319353103638), (-0.3259827196598053, 0.9830834269523621, 1.6388254165649414), (-0.6394380927085876, 0.9830834269523621, 1.543739676475525), (-0.9283202886581421, 0.9830834269523621, 1.389328956604004), (-1.1815276145935059, 0.9830834269523621, 1.1815271377563477), (-1.3893296718597412, 0.9830834269523621, 0.9283197522163391), (-1.5437402725219727, 0.9830833077430725, 0.6394376158714294), (-1.6388258934020996, 0.9830833077430725, 0.3259822726249695), (-1.670932412147522, 0.9830833077430725, -4.0035914139480155e-07), (-1.6388260126113892, 0.9830833077430725, -0.32598307728767395), (-1.5437402725219727, 0.9830833077430725, -0.6394383907318115), (-1.3893296718597412, 0.9830833077430725, -0.9283207058906555), (-1.1815276145935059, 0.9830832481384277, -1.181528091430664), (-0.9283201694488525, 0.9830832481384277, -1.3893301486968994), (-0.6394377946853638, 0.9830832481384277, -1.5437408685684204), (-0.32598230242729187, 0.9830832481384277, -1.6388264894485474), (5.989916758153413e-07, 0.9830832481384277, -1.6709330081939697), (0.325983464717865, 0.9830832481384277, -1.6388262510299683), (0.6394389867782593, 0.9830832481384277, -1.5437403917312622), (0.9283211827278137, 0.9830832481384277, -1.3893295526504517), (1.1815284490585327, 0.9830832481384277, -1.1815273761749268), (1.3893301486968994, 0.9830833077430725, -0.9283198118209839), (1.5437407493591309, 0.9830833077430725, -0.6394373774528503), (1.6388261318206787, 0.9830833077430725, -0.32598182559013367), (1.6709325313568115, 0.9830833077430725, 1.0869399602597696e-06), (1.638825535774231, 0.9830833077430725, 0.3259839415550232), (1.5437394380569458, 0.9830833077430725, 0.6394394040107727), (1.3893283605575562, 0.9830834269523621, 0.9283215403556824), (1.1815260648727417, 0.9830834269523621, 1.1815285682678223), (0.9283185005187988, 0.9830834269523621, 1.389330267906189), (0.6394360065460205, 0.9830834269523621, 1.5437407493591309), (0.3259802758693695, 0.9830834269523621, 1.6388258934020996), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), (0, 16), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-neck", mesh)
    scene.objects.link( obj)


def create_finger_ctrl( scene):
    mesh = bpy.data.meshes.new( "WGT-finger_ctrl.mesh")
    faces = []
    verts = [(-2.9471635798472562e-08, 0.8932003974914551, 0.8988392353057861), (-0.1753549426794052, 0.8932003378868103, 0.8815682530403137), (-0.34397098422050476, 0.8932003378868103, 0.8304190635681152), (-0.49936845898628235, 0.8932003378868103, 0.7473574280738831), (-0.635575532913208, 0.8932003378868103, 0.6355752348899841), (-0.7473577857017517, 0.8932003378868103, 0.4993681311607361), (-0.8304194211959839, 0.8932002782821655, 0.3439706563949585), (-0.8815684914588928, 0.8932002782821655, 0.17535464465618134), (-0.8988394737243652, 0.8932002782821655, -2.0201804318276118e-07), (-0.8815685510635376, 0.8932002782821655, -0.17535504698753357), (-0.8304194211959839, 0.8932002782821655, -0.3439710736274719), (-0.7473577857017517, 0.8932002782821655, -0.4993686378002167), (-0.635575532913208, 0.8932002186775208, -0.6355756521224976), (-0.4993683993816376, 0.8932002186775208, -0.747357964515686), (-0.3439708352088928, 0.8932002186775208, -0.8304197192192078), (-0.1753547042608261, 0.8932002186775208, -0.8815687894821167), (2.63407429201834e-07, 0.8932002186775208, -0.8988397121429443), (0.1753551959991455, 0.8932002186775208, -0.8815686702728271), (0.3439713716506958, 0.8932002186775208, -0.8304194211959839), (0.4993688464164734, 0.8932002186775208, -0.7473576664924622), (0.6355758309364319, 0.8932002186775208, -0.6355752944946289), (0.747357964515686, 0.8932002782821655, -0.4993681311607361), (0.8304196000099182, 0.8932002782821655, -0.34397053718566895), (0.8815685510635376, 0.8932002782821655, -0.17535437643527985), (0.8988394141197205, 0.8932002782821655, 5.980401169836114e-07), (0.881568193435669, 0.8932002782821655, 0.17535553872585297), (0.8304188847541809, 0.8932002782821655, 0.3439716398715973), (0.7473570108413696, 0.8932003378868103, 0.4993690848350525), (0.6355745792388916, 0.8932003378868103, 0.6355760097503662), (0.4993673861026764, 0.8932003378868103, 0.7473580837249756), (0.34396976232528687, 0.8932003378868103, 0.830419659614563), (0.17535348236560822, 0.8932003378868103, 0.8815684914588928), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-finger_ctrl", mesh)
    scene.objects.link( obj)

def create_ball( scene):
    mesh = bpy.data.meshes.new( "WGT-ball.mesh")
    faces = []
    verts = [(0.0, 1.0, 0.0), (-0.19509032368659973, 0.9807852506637573, 0.0), (-0.3826834559440613, 0.9238795042037964, 0.0), (-0.5555702447891235, 0.8314695954322815, 0.0), (-0.7071067690849304, 0.7071067690849304, 0.0), (-0.8314696550369263, 0.5555701851844788, 0.0), (-0.9238795042037964, 0.3826834261417389, 0.0), (-0.9807852506637573, 0.19509035348892212, 0.0), (-1.0, 7.549790126404332e-08, 0.0), (-0.9807853102684021, -0.19509020447731018, 0.0), (-0.9238795638084412, -0.38268327713012695, 0.0), (-0.8314696550369263, -0.5555701851844788, 0.0), (-0.7071067690849304, -0.7071067690849304, 0.0), (-0.5555701851844788, -0.8314696550369263, 0.0), (-0.38268327713012695, -0.9238796234130859, 0.0), (-0.19509008526802063, -0.9807853102684021, 0.0), (3.2584136988589307e-07, -1.0, 0.0), (0.19509072601795197, -0.9807851910591125, 0.0), (0.3826838731765747, -0.9238793253898621, 0.0), (0.5555707216262817, -0.8314692974090576, 0.0), (0.7071072459220886, -0.707106351852417, 0.0), (0.8314700126647949, -0.5555696487426758, 0.0), (0.923879861831665, -0.3826826810836792, 0.0), (0.9807854294776917, -0.1950894594192505, 0.0), (1.0, 9.655991561885457e-07, 0.0), (0.980785071849823, 0.1950913518667221, 0.0), (0.923879086971283, 0.38268446922302246, 0.0), (0.831468939781189, 0.5555712580680847, 0.0), (0.7071058750152588, 0.707107663154602, 0.0), (0.5555691123008728, 0.8314703702926636, 0.0), (0.38268208503723145, 0.9238801002502441, 0.0), (0.19508881866931915, 0.9807855486869812, 0.0), (0.0, 4.172325134277344e-07, 0.9999997019767761), (-0.19509032368659973, 3.5762786865234375e-07, 0.9807849526405334), (-0.3826834559440613, 3.5762786865234375e-07, 0.9238792061805725), (-0.5555702447891235, 3.5762786865234375e-07, 0.8314692974090576), (-0.7071067690849304, 3.5762786865234375e-07, 0.7071064710617065), (-0.8314696550369263, 3.5762786865234375e-07, 0.5555698871612549), (-0.9238795042037964, 3.2782554626464844e-07, 0.3826830983161926), (-0.9807852506637573, 3.2782554626464844e-07, 0.19509004056453705), (-1.0, 3.1292435664909135e-07, -2.3742649091218482e-07), (-0.9807853102684021, 2.980232238769531e-07, -0.19509051740169525), (-0.9238795638084412, 2.980232238769531e-07, -0.38268357515335083), (-0.8314696550369263, 2.980232238769531e-07, -0.5555704832077026), (-0.7071067690849304, 2.384185791015625e-07, -0.7071070671081543), (-0.5555701851844788, 2.384185791015625e-07, -0.8314699530601501), (-0.38268327713012695, 2.384185791015625e-07, -0.9238799214363098), (-0.19509008526802063, 2.384185791015625e-07, -0.980785608291626), (3.2584136988589307e-07, 2.384185791015625e-07, -1.0000003576278687), (0.19509072601795197, 2.384185791015625e-07, -0.9807854890823364), (0.3826838731765747, 2.384185791015625e-07, -0.9238796234130859), (0.5555707216262817, 2.384185791015625e-07, -0.8314695954322815), (0.7071072459220886, 2.384185791015625e-07, -0.7071066498756409), (0.8314700126647949, 2.980232238769531e-07, -0.5555699467658997), (0.923879861831665, 2.980232238769531e-07, -0.3826829791069031), (0.9807854294776917, 2.980232238769531e-07, -0.19508977234363556), (1.0, 3.1292444191421964e-07, 6.52674771117745e-07), (0.980785071849823, 3.2782554626464844e-07, 0.19509103894233704), (0.923879086971283, 3.2782554626464844e-07, 0.3826841711997986), (0.831468939781189, 3.5762786865234375e-07, 0.5555709600448608), (0.7071058750152588, 3.5762786865234375e-07, 0.7071073651313782), (0.5555691123008728, 3.5762786865234375e-07, 0.8314700722694397), (0.38268208503723145, 3.5762786865234375e-07, 0.9238798022270203), (0.19508881866931915, 3.5762786865234375e-07, 0.9807852506637573), (-1.8440186977386475e-07, 3.8370490074157715e-07, 0.9999997019767761), (-1.341104507446289e-07, -0.1950899362564087, 0.9807849526405334), (-1.4901161193847656e-07, -0.38268306851387024, 0.9238792061805725), (-1.7881393432617188e-07, -0.5555698871612549, 0.8314692974090576), (-1.7881393432617188e-07, -0.7071064114570618, 0.7071064710617065), (-1.7881393432617188e-07, -0.8314692974090576, 0.5555698871612549), (-1.7881393432617188e-07, -0.9238791465759277, 0.3826830983161926), (-1.7881393432617188e-07, -0.9807848930358887, 0.19509004056453705), (-1.7881393432617188e-07, -0.9999996423721313, -2.3742649091218482e-07), (-1.1920928955078125e-07, -0.9807849526405334, -0.19509051740169525), (-1.1920928955078125e-07, -0.9238792061805725, -0.38268357515335083), (-1.1920928955078125e-07, -0.8314692974090576, -0.5555704832077026), (-5.960464477539063e-08, -0.7071064114570618, -0.7071070671081543), (-5.960464477539063e-08, -0.5555698275566101, -0.8314699530601501), (-2.9802322387695312e-08, -0.3826828896999359, -0.9238799214363098), (-1.4901161193847656e-08, -0.1950896978378296, -0.980785608291626), (-5.587907025983441e-09, 7.095462706274702e-07, -1.0000003576278687), (1.4901161193847656e-08, 0.195091113448143, -0.9807854890823364), (2.9802322387695312e-08, 0.38268426060676575, -0.9238796234130859), (5.960464477539063e-08, 0.5555710792541504, -0.8314695954322815), (5.960464477539063e-08, 0.7071076035499573, -0.7071066498756409), (0.0, 0.8314703702926636, -0.5555699467658997), (0.0, 0.9238802194595337, -0.3826829791069031), (0.0, 0.9807857871055603, -0.19508977234363556), (0.0, 1.0000004768371582, 6.52674771117745e-07), (0.0, 0.9807854890823364, 0.19509103894233704), (0.0, 0.9238795042037964, 0.3826841711997986), (-5.960464477539063e-08, 0.8314692974090576, 0.5555709600448608), (-5.960464477539063e-08, 0.7071062326431274, 0.7071073651313782), (-5.960464477539063e-08, 0.5555694699287415, 0.8314700722694397), (-8.940696716308594e-08, 0.3826824724674225, 0.9238798022270203), (-1.043081283569336e-07, 0.1950892060995102, 0.9807852506637573), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), (33, 32), (34, 33), (35, 34), (36, 35), (37, 36), (38, 37), (39, 38), (40, 39), (41, 40), (42, 41), (43, 42), (44, 43), (45, 44), (46, 45), (47, 46), (48, 47), (49, 48), (50, 49), (51, 50), (52, 51), (53, 52), (54, 53), (55, 54), (56, 55), (57, 56), (58, 57), (59, 58), (60, 59), (61, 60), (62, 61), (63, 62), (32, 63), (65, 64), (66, 65), (67, 66), (68, 67), (69, 68), (70, 69), (71, 70), (72, 71), (73, 72), (74, 73), (75, 74), (76, 75), (77, 76), (78, 77), (79, 78), (80, 79), (81, 80), (82, 81), (83, 82), (84, 83), (85, 84), (86, 85), (87, 86), (88, 87), (89, 88), (90, 89), (91, 90), (92, 91), (93, 92), (94, 93), (95, 94), (64, 95), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-ball", mesh)
    scene.objects.link( obj)


def create_box( scene):
    mesh = bpy.data.meshes.new( "WGT-box.mesh")
    verts = [(-1.0, -1.0, -1.0), (-1.0, 1.0, -1.0), (1.0, 1.0, -1.0), (1.0, -1.0, -1.0), (-1.0, -1.0, 1.0), (-1.0, 1.0, 1.0), (1.0, 1.0, 1.0), (1.0, -1.0, 1.0), ]
    edges = [(0, 4), (4, 5), (5, 1), (1, 0), (5, 6), (6, 2), (2, 1), (6, 7), (7, 3), (3, 2), (7, 4), (0, 3), ]
    faces = [(4, 5, 1, 0, ), (5, 6, 2, 1, ), (6, 7, 3, 2, ), (7, 4, 0, 3, ), (0, 1, 2, 3, ), (7, 6, 5, 4, ), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-box", mesh)
    scene.objects.link( obj)


def create_face_ctrl( scene):
    mesh = bpy.data.meshes.new( "WGT-face_ctrl.mesh")
    faces = []
    verts = [(-9.11451465412938e-08, 0.3703788220882416, 0.6135372519493103), (-0.11969531327486038, 0.3703787624835968, 0.6017482876777649), (-0.23479072749614716, 0.3703787624835968, 0.5668344497680664), (-0.34086325764656067, 0.3703787624835968, 0.5101375579833984), (-0.43383654952049255, 0.3703787624835968, 0.43383628129959106), (-0.5101378560066223, 0.3703787624835968, 0.340862900018692), (-0.5668347477912903, 0.3703787624835968, 0.2347903996706009), (-0.6017485857009888, 0.3703787624835968, 0.1196950376033783), (-0.6135374903678894, 0.3703787326812744, -1.4866552078274253e-07), (-0.6017485857009888, 0.3703787326812744, -0.11969534307718277), (-0.5668348073959351, 0.3703787326812744, -0.23479071259498596), (-0.5101378560066223, 0.3703787326812744, -0.34086331725120544), (-0.43383654952049255, 0.37037867307662964, -0.4338366687297821), (-0.3408631980419159, 0.37037867307662964, -0.5101379156112671), (-0.2347906082868576, 0.37037867307662964, -0.5668349266052246), (-0.11969517171382904, 0.37037867307662964, -0.6017487049102783), (1.0877073464143905e-07, 0.37037867307662964, -0.6135376691818237), (0.11969536542892456, 0.37037867307662964, -0.6017486453056335), (0.23479080200195312, 0.37037867307662964, -0.5668347477912903), (0.34086334705352783, 0.37037867307662964, -0.510137677192688), (0.4338366985321045, 0.37037867307662964, -0.433836430311203), (0.5101379156112671, 0.3703787326812744, -0.3408629894256592), (0.5668347477912903, 0.3703787326812744, -0.2347903549671173), (0.601748526096344, 0.3703787326812744, -0.11969488114118576), (0.6135373115539551, 0.3703787624835968, 3.9744489299664565e-07), (0.6017482876777649, 0.3703787624835968, 0.11969565600156784), (0.5668343305587769, 0.3703787624835968, 0.23479105532169342), (0.5101372599601746, 0.3703787624835968, 0.34086355566978455), (0.43383580446243286, 0.3703787624835968, 0.4338368773460388), (0.34086236357688904, 0.3703787624835968, 0.5101380348205566), (0.2347896844148636, 0.3703787624835968, 0.5668348670005798), (0.11969420313835144, 0.3703787624835968, 0.6017484664916992), ]
    edges = [(1, 0), (2, 1), (3, 2), (4, 3), (5, 4), (6, 5), (7, 6), (8, 7), (9, 8), (10, 9), (11, 10), (12, 11), (13, 12), (14, 13), (15, 14), (16, 15), (17, 16), (18, 17), (19, 18), (20, 19), (21, 20), (22, 21), (23, 22), (24, 23), (25, 24), (26, 25), (27, 26), (28, 27), (29, 28), (30, 29), (31, 30), (0, 31), ]
    mesh.from_pydata(verts, edges, faces)
    mesh.update()
    mesh.update()
    obj = bpy.data.objects.new( "WGT-face_ctrl", mesh)
    scene.objects.link( obj)
    
class QuickRigCreateWidgets( bpy.types.Operator):
    bl_label = "Create Widgets"
    bl_description = ""
    bl_idname = "quickrig.create_widgets"
    
    def execute( self, context):
        scene = context.scene
        quickrig = scene.quickrig
        
        if quickrig.create_wgt == 'root':
            create_root( scene)
        if quickrig.create_wgt == 'hips':
            create_hips( scene)
        if quickrig.create_wgt == 'torso':
            create_torso( scene)
        if quickrig.create_wgt == 'chest':
            create_chest( scene)
        if quickrig.create_wgt == 'neck':
            create_neck( scene)
        if quickrig.create_wgt == 'head':
            create_head( scene)
        if quickrig.create_wgt == 'shoulder':
            create_shoulder( scene)
        if quickrig.create_wgt == 'fk_arms':
            create_fk_arms( scene)
        if quickrig.create_wgt == 'hands':
            create_hands( scene)
        if quickrig.create_wgt == 'fingers':
            create_fingers( scene)
        if quickrig.create_wgt == 'finger_ctrl':
            create_finger_ctrl( scene)
        if quickrig.create_wgt == 'palm_curl':
            create_palm_curl( scene)
        if quickrig.create_wgt == 'feet':
            create_feet( scene)
        if quickrig.create_wgt == 'foot_roll':
            create_foot_roll( scene)
        if quickrig.create_wgt == 'ik_poles':
            create_ik_poles( scene)
        if quickrig.create_wgt == 'eye_track':
            create_eye_track( scene)
        if quickrig.create_wgt == 'face_ctrl':
            create_face_ctrl( scene)
        if quickrig.create_wgt == 'plus':
            create_plus( scene)
        if quickrig.create_wgt == 'ball':
            create_ball( scene)
        if quickrig.create_wgt == 'box':
            create_box( scene)
        
        return {'FINISHED'}

class QuickRigIKChainFromSelected( bpy.types.Operator):
    bl_description = "Create IK chain from selected bones. Select the base and target bones, run the operator."
    bl_label = "Create IK Chain"
    bl_idname = "quickrig.create_ik"
    
    def execute( self, context):
        ob = context.object
        scene = context.scene
        base_eb = context.selected_bones[0]
        target_eb = context.selected_bones[-1]
        chain_eb = [eb for eb in base_eb.children_recursive if not eb == target_eb]
        # if scene.quickrig.fk_ik:
            # pass
        # else:
        # Just create the constraint
        target_eb.parent = None
        ik_constr = ob.pose.bones[ chain_eb[-1].name].constraints.new(type = 'IK')
        ik_constr.target = ob
        ik_constr.subtarget = target_eb.name
        ik_constr.chain_count = chain_eb.__len__() + 1
        
        return {'FINISHED'}

class QuickRigProperties( bpy.types.PropertyGroup):
    fk_ik = bpy.props.BoolProperty( name = "Create FK/IK", default = True)
    lock_x = bpy.props.BoolProperty( name = "Lock X", default = True)
    lock_y = bpy.props.BoolProperty( name = "Lock Y", default = True)
    lock_z = bpy.props.BoolProperty( name = "Lock Z", default = True)
    lock_w = bpy.props.BoolProperty( name = "Lock W", default = True)
    lock_loc = bpy.props.BoolProperty( name = "Lock Location", default = True)
    lock_rot = bpy.props.BoolProperty( name = "Lock Rotation", default = True)
    lock_scale = bpy.props.BoolProperty( name = "Lock Scale", default = True)
    unlock = bpy.props.BoolProperty( name = "Unlock", default = False)
    deform = bpy.props.BoolProperty( name = "Deform", default = False)
    shape = bpy.props.StringProperty( name = "Shape", default = "")
    # Shape creation.
    create_wgt = bpy.props.EnumProperty( name = "Widget",
                                    items = [
                                    ( 'root', "Root", ""),
                                    ( 'hips', "Hips", ""),
                                    ( 'torso', "Torso", ""),
                                    ( 'chest', "Chest", ""),
                                    ( 'neck', "Neck", ""),
                                    ( 'head', "Head", ""),
                                    ( 'shoulder', "Shoulder", ""),
                                    ( 'fk_arms', "FK Arms", ""),
                                    ( 'hands', "Hands", ""),
                                    ( 'fingers', "Fingers", ""),
                                    ( 'finger_ctrl', "Finger CTRL", ""),
                                    ( 'palm_curl', "Palm Curl", ""),
                                    ( 'feet', "Feet", ""),
                                    ( 'foot_roll', "Foot Roll", ""),
                                    ( 'eye_track', "Eye Track", ""),
                                    ( 'face_ctrl', "Face CTRL", ""),
                                    ( 'ik_poles', "IK Poles", ""),
                                    ( 'plus', "Plus", ""),
                                    ( 'ball', "Ball", ""),
                                    ( 'box', "Box", "")],
                                    default = 'root')
    # Renaming.
    replace_text = bpy.props.StringProperty( name = "Replace", default = "")
    with_text = bpy.props.StringProperty( name = "With", default = "")
    append = bpy.props.StringProperty( name = "Append", default = "")
    prepend = bpy.props.StringProperty( name = "Prepend", default = "")
    name_index = bpy.props.IntProperty( name = "Index", default = 0)
    side = bpy.props.EnumProperty( name = "Side", items = [
                                  ( 'None', "None", ""),
                                  ( '.L', "Left", ""),
                                  ( '.R', "Right", "")],
                                  default = 'None')
    remove_numbering = bpy.props.BoolProperty( name = "Remove Numbering", default = True)
    # Rotation Mode.
    rotation_mode = bpy.props.EnumProperty( name = "Rotation Mode",
                                    description = "",
                                    items = [
                                    ( 'QUATERNION', "Quaternion", ""),
                                    ( 'XYZ', "XYZ", ""),
                                    ( 'XZY', "XZY", ""),
                                    ( 'YXZ', "YXZ", ""),
                                    ( 'YZX', "YZX", ""),
                                    ( 'ZXY', "ZXY", ""),
                                    ( 'ZYX', "ZYX", ""),
                                    ( 'AXIS_ANGLE', "Axis Angle", "")],
                                    default = 'XYZ')
    mch_name = bpy.props.StringProperty( name = "Prefix", default = "") 
    action_name = bpy.props.StringProperty( name = "Name", default = "")       
    action_loc = bpy.props.BoolProperty( name = "Key Location", default = True)
    action_rot = bpy.props.BoolProperty( name = "Key Rotation", default = True)  
    action_scale = bpy.props.BoolProperty( name = "Key Scale", default = True)   
    index = bpy.props.IntProperty( name = "Index", default = 1, min = 1)     

class QuickRigPosePanel( bpy.types.Panel):
    bl_label = "Quick Rig Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "bone"
    
    @classmethod
    def poll( self, context):
        ob = context.object
        if ob is not None:
            return ob.type == 'ARMATURE' and ob.mode == 'POSE'
    
    def draw( self, context):
        layout = self.layout
        scene = context.scene
        quickrig = scene.quickrig
        
        box = layout.box()
        row = box.row()
        row.prop( quickrig, "lock_loc")
        row.prop( quickrig, "lock_rot")
        row.prop( quickrig, "lock_scale")
        
        row = box.row()
        row.prop( quickrig, "lock_x")
        row.prop( quickrig, "lock_y")
        row.prop( quickrig, "lock_z")
        if quickrig.lock_rot:
            row.prop( quickrig, "lock_w")
        box.prop( quickrig, "unlock")
        box.operator( "quickrig.transform_lock", icon = 'LOCKED')
        
        layout.separator()
        
        box = layout.box()
        box.prop( quickrig, "deform", text = "Use Deform")
        box.operator( "quickrig.deform_toggle", icon = 'MOD_SIMPLEDEFORM')
        
        layout.separator()
        
        box = layout.box()
        box.prop_search( quickrig, "shape", scene, "objects")
        box.operator( "quickrig.set_shape", icon = 'OBJECT_DATAMODE')
        
        layout.separator()
        
        box = layout.box()
        box.prop( quickrig, "action_name")
        row = box.row()
        row.prop( quickrig, "action_loc")
        row.prop( quickrig, "action_rot")
        row.prop( quickrig, "action_scale")
        box.operator( "quickrig.create_action", icon = 'ACTION')
        
        layout.separator()
        box = layout.box()
        box.prop( quickrig, "remove_numbering")
        row = box.row()
        row.prop( quickrig, "replace_text")
        row.prop( quickrig, "with_text")
        row = box.row()
        row.prop( quickrig, "prepend")
        row.prop( quickrig, "append")
        row = box.row()
        row.prop( quickrig, "name_index")
        row.prop( quickrig, "side")
        box.operator( "quickrig.rename_bones", icon = 'SORTALPHA')
        
        layout.separator()
        
        box = layout.box()
        box.prop( quickrig, "rotation_mode")
        box.operator( "quickrig.set_rot_mode", icon = 'MAN_ROT')

        layout.separator()

        box = layout.box()
        box.prop( quickrig, "create_wgt")
        box.operator( "quickrig.create_widgets", icon = 'MESH_DATA')

class QuickRigEditPanel( bpy.types.Panel):
    bl_label = "Quick Rig Edit Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "bone"
    
    @classmethod
    def poll( self, context):
        ob = context.object
        if ob is not None:
            return ob.type == 'ARMATURE' and ob.mode == 'EDIT'
    
    def draw( self, context):
        layout = self.layout
        scene = context.scene
        quickrig = scene.quickrig
        
        layout.operator( "quickrig.rename_mirrored", icon = 'SORTALPHA')
        layout.operator( "quickrig.create_parent", icon = 'GROUP_BONE')
        layout.prop( quickrig, "mch_name")
        layout.operator( "quickrig.mirror_constraints", icon = 'CONSTRAINT_BONE')
        layout.operator( "quickrig.create_ik", icon = "POSE_HLT")


class QuickRigBoneConstraintPanel( bpy.types.Panel):
    bl_label = "Quick Rig Constraints"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "bone_constraint"
    
    @classmethod
    def poll( self, context):
        ob = context.object
        result = False
        if ob is not None:
            if ob.type == 'ARMATURE' and ob.mode == 'POSE':
                if context.active_pose_bone is not None:
                    pb = context.active_pose_bone
                    if len( pb.constraints) > 0:
                        result = True
        return result
                    
    
    def draw( self, context):
        layout = self.layout
        scene = context.scene
        quickrig = scene.quickrig
        
        layout.operator( "quickrig.mirror_constraints", icon = 'CONSTRAINT_BONE')
        row = layout.row()
        row.operator( "quickrig.duplicate_constraint")
        row.prop( quickrig, "index")

def register():
    bpy.utils.register_class( QuickRigBoneLock)
    bpy.utils.register_class( QuickRigBoneDeform)
    bpy.utils.register_class( QuickRigProperties)
    bpy.utils.register_class( QuickRigSetShape)
    bpy.types.Scene.quickrig = bpy.props.PointerProperty( type = QuickRigProperties)
    bpy.utils.register_class( QuickRigRenameMirrored)
    bpy.utils.register_class( QuickRigRenameBones)
    bpy.utils.register_class( QuickRigSetRotMode)
    bpy.utils.register_class( QuickRigCreateParent)
    bpy.utils.register_class( QuickRigCreateAction)
    bpy.utils.register_class( QuickRigCreateWidgets)
    bpy.utils.register_class( QuickRigMirrorConstraints)
    bpy.utils.register_class( QuickRigDuplicateConstraints)
    bpy.utils.register_class( QuickRigIKChainFromSelected)
    bpy.utils.register_class( QuickRigPosePanel)
    bpy.utils.register_class( QuickRigEditPanel)
    bpy.utils.register_class( QuickRigBoneConstraintPanel)
    
def unregister():
    bpy.utils.unregister_class( QuickRigIKChainFromSelected)
    bpy.utils.unregister_class( QuickRigBoneLock)
    bpy.utils.unregister_class( QuickRigBoneDeform)
    bpy.utils.unregister_class( QuickRigProperties)
    bpy.utils.unregister_class( QuickRigSetShape)
    bpy.utils.unregister_class( QuickRigRenameMirrored)
    bpy.utils.unregister_class( QuickRigRenameBones)
    bpy.utils.unregister_class( QuickRigSetRotMode)
    bpy.utils.unregister_class( QuickRigCreateParent)
    bpy.utils.unregister_class( QuickRigCreateAction)
    bpy.utils.unregister_class( QuickRigCreateWidgets)
    bpy.utils.unregister_class( QuickRigMirrorConstraints)
    bpy.utils.unregister_class( QuickRigDuplicateConstraints)
    bpy.utils.unregister_class( QuickRigPosePanel)
    bpy.utils.unregister_class( QuickRigEditPanel)
    bpy.utils.unregister_class( QuickRigBoneConstraintPanel)
    del bpy.types.Scene.quickrig
    
if __name__ == "__main__":
    register()
